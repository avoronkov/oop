# Результаты.

## Бастрыкина Алена

[Подсчёт фраз - Ok](/2017.cpp/results/bastrykina/)

[Календарь - Ok](/2017.cpp/results/bastrykina/#2)

[Морской бой - Ok](/2017.cpp/results/bastrykina/#3)

[Жизнь - Ok](/2017.cpp/results/bastrykina/#4)

**Коллоквиум** - `11.5 (max 22.0)`

**Предварительная оценка** - `4`


## Волошина Анастасия

[Подсчёт фраз - Ok](/2017.cpp/results/voloshina/)

[Календарь - Ok](/2017.cpp/results/voloshina/#2)

[Морской бой - Ok](/2017.cpp/results/voloshina/#3)

[Workflow - в процессе](/2017.cpp/results/voloshina/#4)

**Коллоквиум** - `13.5 (max 22.0)`

**Предварительная оценка** - `4-`


## Галиос Максим

[Подсчёт фраз - Ok](/2017.cpp/results/galios/)

[Календарь - Ok](/2017.cpp/results/galios/#2)

[Морской бой - Ok](/2017.cpp/results/galios/#3)

[Жизнь - Ok](/2017.cpp/results/galios/#4)

**Коллоквиум** - `8.5 (max 22.0)`

**Предварительная оценка** - `4`


## Григорович Артём

[Подсчёт фраз - Ok](/2017.cpp/results/grigorovich/)

[Календарь - Ok](/2017.cpp/results/grigorovich/#2)

[Морской бой - Ok](/2017.cpp/results/grigorovich/#3)

[Жизнь - Ok](/2017.cpp/results/grigorovich/#4)

**Коллоквиум** - `9.5 (max 22.0)`

**Предварительная оценка** - `5`


## Петин Кирилл

[Подсчёт фраз - Ok](/2017.cpp/results/petin/)

[Календарь - Ok](/2017.cpp/results/petin/#2)

[Морской бой - Ok](/2017.cpp/results/petin/#3)

[Жизнь - Ok](/2017.cpp/results/petin/#4)

**Коллоквиум** - `13.5 (max 22.0)`

**Предварительная оценка** - `5`


## Пушков Федор

[Подсчёт фраз - Ok](/2017.cpp/results/pushkov/)

[Календарь - Ok](/2017.cpp/results/pushkov/#2)

[Морской бой - Ok](/2017.cpp/results/pushkov/#3)

[Жизнь - Ok](/2017.cpp/results/pushkov/#4)

**Коллоквиум** - `7.5 (max 22.0)`

**Предварительная оценка** - `4`


## Разумов Антон

[Подсчёт фраз - Ok](/2017.cpp/results/razumov/)

[Календарь - Ok](/2017.cpp/results/razumov/#2)

[Морской бой - Ok](/2017.cpp/results/razumov/#3)

[Жизнь - Ok](/2017.cpp/results/razumov/#4)

**Коллоквиум** - `12.0 (max 22.0)`

**Предварительная оценка** - `5`


## Снегирева Екатерина

[Подсчёт фраз - Ok](/2017.cpp/results/snegireva/)

[Календарь - Ok](/2017.cpp/results/snegireva/#2)

[Морской бой - Ok](/2017.cpp/results/snegireva/#3)

[Workflow - Ok](/2017.cpp/results/snegireva/#4)

**Коллоквиум** - `16.0 (max 22.0)`

**Предварительная оценка** - `5`

## Чмиль Александр

[Подсчёт фраз - Ok](/2017.cpp/results/chmil/)

[Календарь - Ok](/2017.cpp/results/chmil/#2)

[Морской бой - Ok](/2017.cpp/results/chmil/#3)

[Workflow - Ok](/2017.cpp/results/chmil/#4)

**Коллоквиум** - `14.0 (max 22.0)`

**Предварительная оценка** - `5`


## Шустова Марина

[Подсчёт фраз - Ok](/2017.cpp/results/shustova/)

[Календарь - Ok](/2017.cpp/results/shustova/#2)

[Морской бой - Ok](/2017.cpp/results/shustova/#3)

[Жизнь - Ok](/2017.cpp/results/shustova/#4)

**Коллоквиум** - `13.5 (max 22.0)`

**Предварительная оценка** - `5`

## Яшин Артём

[Подсчёт фраз - Ok](/2017.cpp/results/yashin/)

[Календарь - Ok](/2017.cpp/results/yashin/#2)

[Морской бой, клиент - Ok](/2017.cpp/results/yashin/#3)

[Морской бой, сервер - Ok](/2017.cpp/results/yashin/#3)

[Жизнь - Ok](/2017.cpp/results/yashin/#4)

**Коллоквиум** - `16.5 (max 22.0)`

**Предварительная оценка** - `5`
