# Гаврилова Дарья (3)

Предварительная оценка - `3`

## Подсчёт фраз (Задача 1). Хорошо.

**Код** - хорошо.

- [Пожелание] Поправить сборку через ant.
```
--- a/build.xml
+++ b/build.xml
@@ -10,9 +10,11 @@
     <property name="lib.dir" value="lib/"/>
 
        <!-- set jar name here -->
-       <property name="jar.path"    value="${jar.dir}/helloworld.jar"/>
+       <property name="jar.path"    value="${jar.dir}/phrases.jar"/>
+       <property name="jar2.path"    value="${jar.dir}/api.jar"/>
        <!-- Change your main class here: -->
-       <property name="main.class"  value="ru.nsu.g.avoronkov.helloworld.Main" />
+       <property name="main.class"  value="ru.nsu.g.dgavrilova.task1.Main" />
+       <property name="main2.class"  value="ru.nsu.g.dgavrilova.task1.MainAPI" />
 
     <path id="classpath.test">
         <pathelement location="${lib.dir}/junit-4.12.jar"/>
@@ -43,6 +45,11 @@
                 <attribute name="Main-Class" value="${main.class}"/>
             </manifest>
         </jar>
+        <jar destfile="${jar2.path}" basedir="${classes.dir}">
+            <manifest>
+                <attribute name="Main-Class" value="${main2.class}"/>
+            </manifest>
+        </jar>
     </target>
 
     <target name="run" depends="jar">
```

- [Пожелание] Класс парсер ищет имя файла всегда на первой позиции.
Я бы исправил как минимум так:
```
--- a/src/ru/nsu/g/dgavrilova/task1/Parser.java
+++ b/src/ru/nsu/g/dgavrilova/task1/Parser.java
@@ -11,8 +11,11 @@ public class Parser {
     }   
 
     public String getFilename() throws NoSuchElementException {
-        if (args.length > 0)
-            return args[0];
+       for (int i=0; i < args.length; i+=2){
+           if (args[i].charAt(0) != '-') {
+               return args[i];
+           }
+       }
         throw new NoSuchElementException("Filename is not found.");
     }   
 
```

**Тесты** - Ok.

## StackPL (Задача 2). Ok.

**Код** - Ok.

- [OK] Не совсем корректная реализация методов `parseCycle` и `parseDefine`.
Текущая позиция передается через аргумент `int pos`, сдвигается внутри метода parseCycle, но при выходе из метода остается прежней (`++i`).
Из-за этого возникает ошибка `java.lang.IllegalArgumentException: Property is not found.`, когда встречается следующий токен `]`.

**Тесты** - Ok.

## Фабрика (Задача 4). Ok.

**Код** - Ok.

- [Ok] Поправить сборку через ant.
