# Плешков Андрей (экз. 5)

Оценка за досрочный экзамен - `5`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - в процессе.

- [Ok] Stream API: `if (entry.getValue() >= m)` внутри forEach лучше заменить на `filter`.

- [Ok] Некорректный вывод в случае, когда количество слов меньше, чем длина фразы.

- [Ok] Stream API: использовать метод `collect` для подсчета фраз в потоке.

**Тесты** - Ok.

- [Ok] Тесты упали со странной ошибкой (возможно дело в junit5):
```
    [javac] skipping symbolic link /sys/devices/pci0000:00/0000:00:00.0/subsystem/devices/0000:00:00.0/subsystem/devices/0000:00:00.0/subsystem/devices/0000:00:00.0/subsystem/devices/0000:00:00.0/subsystem/devices/0000:00:01.3/firmware_node/subsystem/devices/device:19/subsystem/devices/device:19/subsystem/devices/device:19/subsystem/devices/device:19/subsystem/devices/PNP0501:02/physical_node/subsystem/devices/00:00/subsystem/devices/00:00/subsystem/devices/00:00/subsystem/devices/00:00/subsystem/devices/00:05/driver/00:05/tty/ttyS2/subsystem/tty56/subsystem/ttyS0/device/subsystem -- too many levels of symbolic links.
```

## Stack PL (Задача 2). Ok.

**Код** - Ok.

- [Ok] Вместо `SyntaxInterpreter.readProperties` следует использовать `java.util.Properties`.

**Тесты** - Ok.

## Фабрика (Задача 4). Ok.

**Код** - Ok.

## Игра (Задача 3). Ok.

**Код** - Ok.

**Демонстрация** - Ok.
