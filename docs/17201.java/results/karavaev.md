# Караваев Григорий (экз. 5)

Оценка за досрочный экзамен - `5`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Scanner должен быть закрыт (метод `close()`)

- [Ok] Лучше не использовать `"not a file"` в качестве специального значения (лучше использовать `-` или вообще обойтись без специальных значений, например булевым флагом isFile).

- [Ok] Stream API. Фильтрацию элементов и вывод в stdout также можно сделать средствами Stream API.

- [Ok] Если не указаны аргументы, должны использоваться значения по умолчанию. Сейчас выводится сообщение:
```
-Filename not found
-Argument "-n" not found
-Argument "-m" not found
```

- [Ok] Некорректный формат вывода (частота должна выводиться в скобках):
```
-yellow submarine 6
-submarine yellow 4
+yellow submarine (6)
+submarine yellow (4)
```

- [Ok] Добавить в `build.xml` сборку jar-архива для stream-api версии программы.

**Тесты** - Ok.

- [Ok] Кажется, тесты не должны зависить от вида конца строк (`\n` vs. `\r\n`).
Один из вариантов - использовать `replaceAll("\r\n", "\n")` перед сравнением.

- [Ok] Вместо `System.out.println("Test failed");` лучше написать:
```
import static junit.framework.TestCase.fail;
...
	fail("Test failed: " + e.toString());)
```

## Stack PL (Задача 2). Ok.

**Код** - Ok.

- [Ok] В `OperationFactory` для разбора файла конфигурации следует использовать `java.util.Properties`

- [Ok] `operations.txt` не копируется внутрь jar-файла.

- [Ok] В `operations.txt` не должны присутсвовать операции sqrt, print, то есть те, для которых можно "вычислить" имена классов.

- [Ok] Реализовать ленивую загрузку классов, то есть класс должен загружаться в момент первого использования.
(допустимо загружать классы `+`, `-` ... при старте программы).

**Тесты** - Ok.

## Фабрика (Задача 4). Ok.

**Код** - Ok.

## Чат (Задача 5). Ok.

**Код** - Ok.

- [Ok] добавить xml версию.

- [Ok] Compile error:
```
    [javac] /home/alxr/study/oop/charts/repos/karavaev/java-chat.git/src/ru/nsu/g/karavaev/g/chat/XMLVersion/XMLServerReadThread.java:3: error: package javafx.util does not exist
    [javac] import javafx.util.Pair;
```

## Игра (Задача 3). Ok.

**Код** - Ok.

- [Ok] Добавить javafx библиотеки, чтобы сделать код компилируемым.

- [Ok] Убрать из model зависимости на графическую библиотеку (javafx).

- [Ok] Циклическая зависимость между пакетами `model` и `view`:
```
model/Sprite.java:
import ru.nsu.g.karavaev.g.game.view.SpriteImage;

view/View.java:
import ru.nsu.g.karavaev.g.game.model.*;
```

- [Пожелание] По хорошему, следует избавиться от зависимости `model.GameModel -> controller.GameController`,
поскольку последний зависит от javafx.
В крайнем случае - подробно описать, как это следует делать.
