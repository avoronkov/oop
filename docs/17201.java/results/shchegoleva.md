# Щеголева Ангелина (5)

Предварительная оценка - `5`

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] `public class phrases_counter`: классы должны именоваться с большой буквы без символа прочерка `_`.
(т.е. `PhrasesCounter).

- [Ok] Добавить реализацию со Stream API в сборку.

- [Ok] Вывод программы должен осуществляться в stdout.

- [Пожелание] Писать ошибки в стандартный поток ошибок (`System.err`), возвращать ненулевой код возврата при ошибке (`System.exit(1)`)

**Тесты** - Ok.

## Stack PL (Задача 2). Ok.

**Код** - Ok.

- [Ok] В `build.xml` нужно указать правильное значение для `property name="main.class"`.

- [Ok] В случае ошибки в перегруженных методах `operate` ("пустой стэк") нужно выбрасывать исключение вместо сообщения на экран и `return`.

- **Upd** в интерфейсе `Operation` метод `operate` должен быть задекларирован, как выбрасывающий исключение:
```
	public void operate(Stack<Integer> stk) throws (Exception);
```
в реализации этого метода в классах операции выбрасывать исключение, если стек пустой.

- [Ok] в главном цикле `Interpret.interpret` не дожен фигурировать список доступных операций:
```
} else if ("sqrt".equals(operationName) || "print".equals(operationName) || "dup".equals(operationName) || "swap".equals(operationName) || "rot".equals(operationName) || "drop".equals(operationName)) {
```
[Ok] Вместо этого должна быть следующая логика:

* If слово является числом - добавить число на стек;

* Else if слово равно `define` - обработать define;

* Else if слово равно `[` - обработать цикл;

* Иначе - попробовать загрузить операцию с данным именем, если не получилось - выбрасывается исключение, выполнение программы прерывается.

- [Ok] Interpret.cycle: Циклы не нужно помещать в `dictionary`, соответственно статическая переменная `cycleCounter` не нужна.
(P.s. статические неконстантные поля классов - это почти всегда плохо).

**Тесты** - Ok.

## Чат (Задача 5). Ok.

**Код** - Ok.

- [Ok]Добавить сериализацию сообщений (при передаче по сети).

- [Пожелание] Добавить выбор ip адреса сервера на клиенте.
Лучше через аргумент командной строки, или через UI. (Чтение из stdin неинтуитивно).

- [Ok] Показывать новым клиентам историю сообщений (последние 10 сообщений).

- [Ok] Добавить xml-сериализацию.

- [Ok] Сделать сборку jar клиента и сервера в build.xml

- [Ok] Добавить клиент для xml-версии.

## Сапёр (Задача 3). fine.

**Код** - fine.

- [fine] Починить сборку jar-файла. (Main-class + скопировать ресурсы).
**Upd** Main-класс всё ещё неправильный.

- [Ok] Пакет `controller` на самом деле - модель.

- [пожелание] Ошибка запуска:
```
$ java -jar ./build/jar/minesweeper.jar
Exception in thread "main" java.lang.NullPointerException
        at java.desktop/javax.swing.ImageIcon.<init>(ImageIcon.java:217)
        at ru.nsu.g.ashshegoleva1.minesweeper.view.ViewMinesweeper.getImage(Unknown Source)
        at ru.nsu.g.ashshegoleva1.minesweeper.view.ViewMinesweeper.setImages(Unknown Source)
        at ru.nsu.g.ashshegoleva1.minesweeper.view.ViewMinesweeper.<init>(Unknown Source)
        at ru.nsu.g.ashshegoleva1.minesweeper.Main.main(Unknown Source)
```

## Фабрика (Задача 4). Ok.

**Код** - Ok.

- [Ok] Починить jar (`conf.properties` должен быть в пакете `src/ru/nsu/g/ashshegoleva1/factory`)

- [Ok] Убрать `java.lang.invoke.StringConcatFactory`

- [Ok] Сделать общий generic класс для всех `*Storage`
