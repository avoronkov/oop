# Кулыгин Максим (экз. 3)

Оценка за досрочный экзамен - `3`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Если считываем из консоли, то чтение продолжается до конца файла (`EOF`).
При чтении из консоли символ конца файла можно передать через `ctrl+d` на линиксу и `ctrl+z` на Windows.

- [Ok] Открытые потоки ввода следует закрывать.

- [Ok] Необходимо сделать "обычную" и "stream api" реализации в рамках одного проекта.

- [Ok] Фразы должны быть отсортированы по убыванию частоты.

- [Ok] В `PhraseCounter.java: private void printPhrases()` лишний код `System.out.println(");`

- [Пожелание] Сообщения об ошибках (и отладочную информацию) следует писать в `System.err`.

**Тесты** - Ok.

- [Ok] Добавить тесты.

## Stack PL (Задача 2). Ok.

**Код** - Ok.

- [Ok] `private static Properties properties` - убрать static.

- [Ok] `OperationFactory.java`: сохранять созданный объект операции в `Map<String, Operation>` для повторного использования.

- [Ok] Отладочные сообщения следует писать на поток ошибок `System.err`:
```
Swap first time loaded
Dup first time loaded
Dup got from map
...
```

**Тесты** - Ok.

- [Ok] Ошибка компиляции:
```
    [javac] /home/alxr/study/oop/charts/repos/kulygin/task2.git/test/ru/nsu/g/mkulygin/task2/HelloTest.java:12: error: package ru.nsu.g.avoronkov.helloworld does not exist
    [javac] import ru.nsu.g.avoronkov.helloworld.Hello;
    [javac]                                     ^
    [javac] /home/alxr/study/oop/charts/repos/kulygin/task2.git/test/ru/nsu/g/mkulygin/task2/HelloTest.java:20: error: cannot find symbol
    [javac]                     Hello hello = new Hello();
    [javac]                     ^
```

## Сапёр (Задача 3). Ok.

**Код** - Ok.

- [Ok] Добавить `build.xml` со сборкой text и gui версий.
