# Петин Кирилл

Предварительная оценка - `4`.

Доклад "Ввод/вывод" +

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Если в `r.printPhrases(in,out,length.value,repeats.value);` вылетит исключение, то `in` и `out` не закроются.

- [Ok] интеграционный тест пройден

**Тесты** - Ok.
```
   [junit] Tests run: 3, Failures: 0, Errors: 0, Time elapsed: 0.047 sec
   [junit] Tests run: 3, Failures: 0, Errors: 0, Time elapsed: 0.047 sec
```

[repo](https://bitbucket.org/petin_oop/jtask1)

(проверено 24 марта)

## StackPL (Задача 2). Ok.

## Сапер (Задача 3). Ok.
