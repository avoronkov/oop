# Шустова Марина

Предварительная оценка - `4`.

Доклад "Jenerics" +

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Вместо использования статических переменных лучше передавать данные
как аргументы и возвращаемые значения методов. Например:
```
static Map<String, Integer> mkMap(Vector<String> input){ ... }
```

- [Ok] `Scanner` нужно закрывать (например через try-with-resource).

- [Ok] Добавить `build.xml`

- [Ok] Сортировка должна быть в обратном порядке (от более частых к менее).

**Тесты** - Ok.

[repo](https://bitbucket.org/shustova_oop/java_phrases)

(проверено 16 июня)

## Stack PL (Задача 2). Ok.

**Код** - Ok.

- [Ok] Отсутствует `build.xml`

**Тесты** - Ok.

[repo](https://bitbucket.org/shustova_oop/calc)

(проверено 16 июня)

## Сапёр (Задача 3). Ок.

**Код** - Ok.

[repo](https://bitbucket.org/account/user/shustova_oop/projects/MIN)

(проверено 16 июня)

## Фабрика (Задача 4). Ок.

**Код** - Ok.

[repo](https://bitbucket.org/shustova_oop/factory)

(проверено 16 июня)
