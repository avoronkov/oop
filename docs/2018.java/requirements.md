# Требования к сдаче задач

- [build.xml](/2018.java/seminar6/) для сборки с помощью ant с подднржкой целей `jar` и `test`.

- Структура проекта:
```
build.xml
src/...
lib/
	junit-4.12.jar	
	hamcrest-core-1.3.jar
tests/...
```
(тесты могут находиться как в отдельной директории, так и в директории `src/`)

Скачать junit.jar и hamcrest.jar можно скачать [здесь](https://github.com/junit-team/junit4/wiki/Download-and-Install).

## Hello project with tests.

Пример helloworld проекта с тестами можно посмотреть [здесь](https://github.com/avoronkov/java-examples/tree/master/hello)
