# Результаты.

## Бастрыкина Алена

Предварительная оценка - `4`.

Доклад "Массивы" +

[Подсчёт фраз - Ok](/2018.java/results/bastrykina/)

[Stack PL - Ok](/2018.java/results/bastrykina/#2)

[Змейка - Ok](/2018.java/results/bastrykina/#3)

[Фабрика - Ok](/2018.java/results/bastrykina/#4)

[charts](/charts.16203/bastrykina/index.html)


## Волошина Анастасия

Предварительная оценка - `4`.

Доклад "Объекты, ссылки" +

[Подсчёт фраз - Ok](/2018.java/results/voloshina/)

[Stack PL - ok](/2018.java/results/voloshina/#2)

[Тетрис - ok](/2018.java/results/voloshina/#3)

[Фабрика - ok](/2018.java/results/voloshina/#4)

[charts](/charts.16203/voloshina/index.html)


## Галиос Максим

Предварительная оценка - `3`.

Доклад "Исключения" +

[Подсчёт фраз - хорошо](/2018.java/results/galios/)

[StackPL - хорошо](/2018.java/results/galios/#2)

[Сапер - хорошо](/2018.java/results/galios/#3)

[charts](/charts.16203/galios/index.html)


## Григорович Артём

Предварительная оценка - `4`.

Доклад "Ввод/вывод" +

[Подсчёт фраз - хорошо](/2018.java/results/grigorovich/)

[Stack PL - хорошо](/2018.java/results/grigorovich/#2)

[Сапер - хорошо](/2018.java/results/grigorovich/#3)

[Фабрика - Ok](/2018.java/results/grigorovich/#4)

[Чат - Ok](/2018.java/results/grigorovich/#5)

[charts](/charts.16203/grigorovich/index.html)


## Петин Кирилл

Предварительная оценка - `3`.

Доклад "Наследование" +

[Подсчёт фраз - Ok](/2018.java/results/petin/)

[StackPL - Ok](/2018.java/results/petin/#2)

[Сапер - Ok](/2018.java/results/petin/#3)

[charts](/charts.16203/petin/index.html)


## Пушков Федор

Предварительная оценка - `3`.

Доклад "Коллекции" +

[Подсчёт фраз - в процессе](/2018.java/results/pushkov/)

[StackPL - в процессе](/2018.java/results/pushkov/#2)

[Сапер - хорошо](/2018.java/results/pushkov/#3)

[charts](/charts.16203/pushkov/index.html) 


## Разумов Антон

Предварительная оценка - `5`.

Доклад "Интерфейсы" +

[Подсчёт фраз - Ok](/2018.java/results/razumov/)

[Stack PL - Ok](/2018.java/results/razumov/#2)

[Minesweeper - Ok](/2018.java/results/razumov/#3)

[Factory - Ok](/2018.java/results/razumov/#4)

[Чат - Ok](/2018.java/results/razumov/#5)

[charts](/charts.16203/razumov/index.html)


## Снегирева Екатерина

Предварительная оценка - `5-`.

Доклад "Сборка мусора" +

[Подсчёт фраз - ok](/2018.java/results/snegireva/)

[Stack PL - ok](/2018.java/results/snegireva/#2)

[Сапёр - ok](/2018.java/results/snegireva/#3)

[Фабрика - ok](/2018.java/results/snegireva/#4)

[Чат - Ok](/2018.java/results/snegireva/#5)

[charts](/charts.16203/snegireva/index.html)


## Чмиль Александр

Предварительная оценка - `5`.

Доклад "Try-with-resource" +

[Подсчёт фраз - Ok](/2018.java/results/chmil/)

[Stack PL - Ok](/2018.java/results/chmil/#2)

[Сапер - Ok](/2018.java/results/chmil/#3)

[Фабрика - Ok](/2018.java/results/chmil/#4)

[Чат - Ok](/2018.java/results/chmil/#5)

[charts](/charts.16203/chmil/index.html)


## Шустова Марина

Предварительная оценка - `4`.

Доклад "Jenerics" +

[Подсчёт фраз - Ok](/2018.java/results/shustova/)

[Stack PL - Ok](/2018.java/results/shustova/#2)

[Сапёр - Ok](/2018.java/results/shustova/#3)

[Фабрика - Ok](/2018.java/results/shustova/#4)

[charts](/charts.16203/shustova/index.html)


## Яшин Артём

Предварительная оценка - `5`.

Доклад "Stream API" +

[Подсчёт слов - Ok](/2018.java/results/yashin/)

[Stack PL - Ok](/2018.java/results/yashin/#2)

[Minesweeper - Ok](/2018.java/results/yashin/#3)

[Фабрика - Ok](/2018.java/results/yashin/#4)

[Сетевая игра - Ok](/2018.java/results/yashin/#5)

[charts](/charts.16203/yashin/index.html)


## Суммарная активность

[charts](/charts.16203/_all/index.html)
