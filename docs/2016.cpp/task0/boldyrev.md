# Болдырев Егор.

## Начало (Задача 0). ОК.

- Код - функции `filter()` и `extractField()` работают с потоками ввода-вывода (streams), а не с именами файлов - ОК.
Программа в процессе работы не создает временные файлы - ОК.
Работает `filter` по неполной подстроке (исправлена ошибка в `task1/task1/lib_patternwork.cpp`) - ОК.

- Тесты - в качестве источника входных данных вместо файлов лучше использовать `stringstream`, инициализированный тестовыми данными - ОК.
Тесты `filter_test.test2`, `filter_test.test3` проходят.

### Пожелания.

Вместо специальной строки, символизирующей конец потока ввода (`0`), лучше посылать в терминал символ конца файла `EOF` (`ctrl-d` на linux, `ctrl-z` на windows).
