# Харисов Дамир.

## Начало (Задача 0). ОК.

**Код** - ОК

**Тесты** - ОК

## Тритсет (Задача 1). ОК.

**Код** - ОК.

- Непримитивные типы (объекты) лучше передавать по (константной) ссылке, чем по значнию.
Особенно в случае оператора присваивания: `TritCell_t& operator =(TritCell_t);` - well, ok.

- Немодифицируемые объекты лучше передавать в метод/фуккцию через константную ссылку (напр. `operator<<`) - let it be.

**Тесты** - ОК

### Пожелания.

- Вместо 
```C++
if (left.getTrit() == right) {
	return true;
}   
return false;
```

можно написать проще: `return left.getTrit() == right;`

- Запись
```C++
return buf;
buf.Triset::~Triset();
```

не имеет особого смысла.

- Один из операторов `==`, `!=` можно реализовать через другой.

- Не забывать про константные ссылки в аргументах и константные методы.
