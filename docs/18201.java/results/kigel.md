# Кигель Мария

Предварительная оценка - `3`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Добавить в репозиторий `lib/junit-4.12.jar` и `lib/hamcrest-core-1.3.jar` из проекта `helloworld-java`.

- [Ok] Переименовать `hamcrest-core-1.3(1).jar` в `hamcrest-core-1.3.jar`

- [Ok] Добавить `.gitlab-ci.yml` из `helloworld-java`.

- [Ok] В `build.xml` поправить значения для `main.class`, `APImain.class`.
Также поправить `destfile` и `value` для цели `APIjar`.

- [Ok] Программа зависает, если в качестве агрумента подать имя файла.
 
- [Ok] `Can't find this argument` в выводе не нужен.

- [Ok] Кажется, значения арзументов `-m` и `-n` перепутаны.

- [Ok] Stream-API версия программы падает если передать аргумент `-m` 
```
$ java -jar lab1API.jar -m 3 somefile.txt
Can't find this argument
Exception in thread "main" java.lang.NullPointerException
        at ru.nsu.g.kigel.m.phrases.API_Phrases.lambda$Print_API$1(Unknown Source)
        at java.base/java.util.stream.ReferencePipeline$2$1.accept(ReferencePipeline.java:176)
        at java.base/java.util.stream.SortedOps$SizedRefSortingSink.end(SortedOps.java:357)
        at java.base/java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:485)
        at java.base/java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:474)
        at java.base/java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
        at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
        at java.base/java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
        at java.base/java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:497)
        at ru.nsu.g.kigel.m.phrases.API_Phrases.Print_API(Unknown Source)
        at ru.nsu.g.kigel.m.phrases.API_Main.main(Unknown Source)
```

- [Ok] Неправильные значения для Main-классов в build.xml

- [Ok] Если имя файла не указано, то нужно читать данные с `stdin` (точно так же, как когда указан `-`). Всё остальное работает.


**Тесты** - Ok.

- [Ok] Обновлено: Если тесты расположены в папке `src` вместе с кодом, то в `build.xml` нужно поменять значение `test.src.dir` на `src.dir`.

- [Ok] Если тестовые классы называются `TestSomething`, то в `build.xml` нужно поменять `fileset` для цели `test` с `"**/*Test.class"` на `"**/Test*.class""`.


## StackPL (Задача 2). Ok.

**Код** - Ok.

- [Ok] Загрузка классов операций не работает.
```
> java -jar stackplKigel.jar -
1 2 + print
Exception in thread "main" java.lang.IllegalArgumentException
        at ru.nsu.g.kigel.m.stackpl.OperationFactory.getOperation(Unknown Source)
        at ru.nsu.g.kigel.m.stackpl.Calculator.calc(Unknown Source)
        at ru.nsu.g.kigel.m.stackpl.Main.main(Unknown Source)
```

- [Ok] Кажется, программа всегда печатает
```
null
null
```

- [Ok] Программа `5 dup [ print 1 - dup ]` зависает.

**Тесты** - Ok.

## Arkanoid (Задача 3). Ok.

**Код** - Ok.

- [Ok] Поправить обработку событий keyPressed/keyReleased.
