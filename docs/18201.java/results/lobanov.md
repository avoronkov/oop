# Лобанов Иван

Предварительная оценка - `3`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Добавить в `build.xml` возможность сборки второго jar-архива (для stream-API)

- [Ok] Неправильный `main.class` в `build.xml`:
```
$ java -jar build/jar/helloworld.jar
Error: Could not find or load main class ru.nsu.g.ilobanov.phrases
Caused by: java.lang.ClassNotFoundException: ru.nsu.g.ilobanov.phrases
```

- build.xml: `streamjar.dir` не определен, но и не нужен - вместо него можно использовать `jar.dir`.
`streamjar.path` не определен, но есть `streamjarjar.path`.
```
-    <property name="streamjarjar.path"    value="${streamjar.dir}/phrases.jar"/>
+    <property name="streamjar.path"    value="${jar.dir}/stream-phrases.jar"/>
...
-        <mkdir dir="${streamjar.dir}"/>
+        <mkdir dir="${jar.dir}"/>
```

- [Ok] "Обычная" версия зависает, если в качестве аргумента передать имя фйла.

- [Ok] Аналогично со stream-версией.

**Тесты** - Ok.

## StackPL (Задача 2). Ok.

**Код** - Ok.

**Тесты** - Ok.

## Фабрика (Задача 4). Ok.

**Код** - Ok.

- [Ok] Добавить `build.xml` для сборки jar-архива и запуска тестов.
