# Гнездилова Анна

Предварительная оценка - `3`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Код программы должен располагаться внутри пакета `ru.nsu.g.agnezdilova.phrases`.

- [Ok] Ошибка запуска:
```
$ java -jar Main-stream-Api-1.0-SNAPSHOT.jar 
Error: Could not find or load main class MainStreamAPI
Caused by: java.lang.ClassNotFoundException: MainStreamAPI

$ java -jar phrases-1.0-SNAPSHOT.jar
Error: Could not find or load main class MainUsual
Caused by: java.lang.ClassNotFoundException: MainUsual
```

**Тесты** - OK.

## StackPL (Задача 2). Ok.

**Код** - Ok.

- [Ok] Ошибка запуска:
```
$ ./gradlew jar
$ java -jar StackPL-1.0-SNAPSHOT.jar 
Error: Could not find or load main class Application
Caused by: java.lang.ClassNotFoundException: Application
```

- [Ok] Ресурсы (`commands.txt`) должны быть запакованы в jar-архив и использоваться посредством `Class.getResourseAsStream`.

**Тесты** - Ok.

## Game (Задача 3). Ok.

**Код** - Ok.

- [Ok] Ошибка запуска:
```
$ ./gradlew jar
$ java -jar build/libs/Hello\ World-1.0-SNAPSHOT.jar
Error: Could not find or load main class Application
Caused by: java.lang.ClassNotFoundException: Application
```

- [Ok] Ресурсы (картинки и прочее) должны быть запакованы в jar-архив и использоваться посредством `Class.getResourseAsStream`.

- [Ok] Приложение использует разделители путей Windows (обратный слэш `\\`), лучше использовать `File.separator`.
