# Мичуров Михаил

Предварительная оценка - `5` (автомат).

Презентация. Ok.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Оба варианта зависают при чтении из файла, т.е. при запуске:
```
java -jar build/libs/phrase-counter-stream-api.jar ./testdata/input01.txt
```

**Тесты** - Ok.

## Stack PL (Задача 2). OK.

**Код** - Ok.

**Тесты** - Ok.

## Minesweeper (Задача 3). Ok.

**Код** - Ok.

- [Ok] Ошибка компиляции:
```
GameBoard.java:3: error: cannot find symbol
import ru.nsu.g.mmichurov.minesweeper.Board;
                                     ^
  symbol:   class Board
  location: package ru.nsu.g.mmichurov.minesweeper
1 error
```

- [Ok] Программа падает после нажатия на кнопку `Start` - не могут загрузиться иконки.
Их надо добавить в репозиторий и сделать так, чтобы они паковались в jar-архив.
```
Exception in thread "AWT-EventQueue-0" java.lang.NullPointerException
        at ru.nsu.g.mmichurov.minesweeper.view.gui.GameWindow$IconManager.loadIcons(GameWindow.java:119)
        at ru.nsu.g.mmichurov.minesweeper.view.gui.GameWindow$IconManager.<init>(GameWindow.java:110)
        at ru.nsu.g.mmichurov.minesweeper.view.gui.GameWindow.<init>(GameWindow.java:27)
...
```

- [Ok] Сделать окно растягиваемым.

- [Ok] Добавить консольный интерфейс.

**Тесты** - Ok.

## Factory (Задача 4). Ok.

**Код** - Ok

## Chat (Задача 5). Ok.

**Код** - Ok

- Server - OK, Client - OK

- Object serialization - OK, JSON serialization - OK.
