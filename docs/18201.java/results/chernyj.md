# Чёрный Никита

Предварительная оценка - `3`.

Презентация. Ok.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Добавить `build.xml` из проекта `helloworld-java` или `gradle` скрипт для сборки.

- [Ok] Тесты используют junit5, в `helloworld-java` используется junit4, это приводит у некоторым проблемам при запуске тестов.

- [Ok] Программа должна выводить фразы по убыванию (!) частоты встречаемости.

- [Ok] Программа падает, если передать в качестве имени файла `-` (ожидается чтение из stdin)
**upd** Все ещё падает:
```
java.io.FileNotFoundException: - (No such file or directory)
        at java.base/java.io.FileInputStream.open0(Native Method)
        at java.base/java.io.FileInputStream.open(FileInputStream.java:212)
        at java.base/java.io.FileInputStream.<init>(FileInputStream.java:154)
        at java.base/java.io.FileInputStream.<init>(FileInputStream.java:109)
        at Main.main(Unknown Source)
```

**Тесты** - Ok.

## StackPL (Задача 2). Ok.

**Код** - Ok.

- [Ok] Добавить `build.xml` из проекта `helloworld-java` или `gradle` скрипт для сборки.

- [Ok] `mvn package` падает с ошибкой:
```
[ERROR] Failed to execute goal org.apache.maven.plugins:maven-compiler-plugin:3.1:testCompile (default-testCompile) on project Lab_2_StackCalculator: Compilation failure: Compilation failure: 
[ERROR] /home/alxr/study/oop/charts/repos/chernyj/java_lab2_stackcalculator.git/src/test/java/ru/nsu/g/chernyj/n/calculatorTests/PipelineTest.java:[3,29] package org.junit.jupiter.api does not exist
[ERROR] /home/alxr/study/oop/charts/repos/chernyj/java_lab2_stackcalculator.git/src/test/java/ru/nsu/g/chernyj/n/calculatorTests/PipelineTest.java:[4,29] package org.junit.jupiter.api does not exist
[ERROR] /home/alxr/study/oop/charts/repos/chernyj/java_lab2_stackcalculator.git/src/test/java/ru/nsu/g/chernyj/n/calculatorTests/PipelineTest.java:[7,6] cannot find symbol
[ERROR]   symbol:   class Test
[ERROR]   location: class ru.nsu.g.chernyj.n.calculatorTests.PipelineTest
[ERROR] /home/alxr/study/oop/charts/repos/chernyj/java_lab2_stackcalculator.git/src/test/java/ru/nsu/g/chernyj/n/calculatorTests/PipelineTest.java:[9,9] cannot find symbol
[ERROR]   symbol:   variable Assertions
[ERROR]   location: class ru.nsu.g.chernyj.n.calculatorTests.PipelineTest
[ERROR] -> [Help 1]
```

**Тесты** - Ok.

## Factory (Задача 4). Ok.

**Код** - Ok.

- [Ok] Добавить `build.xml` из проекта `helloworld-java` или `gradle` скрипт для сборки. (`mvn package` собирает jar-архив).

- [Ok] Есть ли способ изменать конфигурацию фабрики?
