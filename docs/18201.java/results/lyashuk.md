# Ляшук Яков

Предварительная оценка - `5` (автомат).

Презентация. Ok.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- (Пожелание) Сообщения об ошибках лучше писать в `System.err`.

**Тесты** - Ok.

## Stack PL (Задача 2). Ok.

**Код** - Ok.

- [OK] Добавить `build.xml` и `.gitlab-ci.yml` в репозиторий.

**Тесты** - Ok.

## Minesweeper (Задача 3). Ok.

**GUI** - Ok.

-[Ok]  В `.../model/Stopwatch.java` лучше использовать `java.util.Timer` вместо `javax.swing.Timer`.

- [OK] Поправить имена Main классов в build.xml

**TextUI - OK

- [Ok] Добавить консольную версию игры.

## Фабрика (Задача 4). Ok.

**Код** - Ok.

- [Ok] Поправить опцию `mainGUI.class` в `build.xml`

## Chat (Задача 5). Ok.

**Код** - Ok.

- [Ok] Необходимо поправить `build.xml`, чтобы json-версия правильно собиралась:

1) Добавить `<pathelement location="${lib.dir}/gson-2.8.6.jar"/>` в `"classpath.test"`.

2) Добавить библиотеку gson внутрь собираемого jar-архива. Я нашел такой способ: [link](https://stackoverflow.com/a/28200904).

```
@@ -60,6 +61,7 @@
     <target name="jarJSONClient" depends="compile">
         <mkdir dir="${jar.dir}"/>
         <jar destfile="${jarJSONClient.path}" basedir="${classes.dir}">
+            <zipgroupfileset dir="${lib.dir}" includes="gson-2.8.6.jar" />
             <manifest>
                 <attribute name="Main-Class" value="${mainJSONClient.class}"/>
             </manifest>
@@ -69,6 +71,7 @@
     <target name="jarJSONServer" depends="compile">
         <mkdir dir="${jar.dir}"/>
         <jar destfile="${jarJSONServer.path}" basedir="${classes.dir}">
+            <zipgroupfileset dir="${lib.dir}" includes="gson-2.8.6.jar" />
             <manifest>
                 <attribute name="Main-Class" value="${mainJSONServer.class}"/>
             </manifest>
```
