# Баранов Илья

Предварительная оценка - `4` (автомат).

Презентация. Ok.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Думаю, самый простой способ использовать `Pair`, это написать класс `Pair` самому, т.к. у него достаточно тривиальная реализация.
```
.../Phrase_calculator.java:2: error: package javafx.util does not exist
import javafx.util.Pair;
                  ^
```
- [Ok] `Working directory: ...` в выводе не нужен.

- [Ok] Программы падают с null-pointer exception если запускать их без аргументов.
(Ожидаемое поведение - чтение из stdin).

- [Ok] `kek!` в выводе не нужен.

- [Пожелание] Использовать правильное именование пакета (`ru.nsu.g...`).

**Тесты** - Ok.

## Stack PL (Задача 2). Ok.

**Код** - Ok.

- [Ok] `Type your command below:` в выводе не нужен (можно написать эту строчку в `System.err`)

- [Ok] Разместить программу в "правильном" пакете (`ru.nsu.g.ibaranov.phrases`).

- [Ok] Загружаемые операции разместить в отдельном подпакете (`operations`).

- [Ok] `Operations.properties` должен содержать имена классов только для операторов (`+`, `-`, `<` ...).
Остальные имена классов должны вычисляться по принципу `operation -> Operation`.

**Тесты** - Ok.

## Snake (Задача 3). Ok.

**Код** - Ok.

- [Ok] Если файл `Statictics.txt` не найден, программа должна его создавать и не падать.

- [Ok] Змейка двигается рывками, что портит игровой процесс :(
Возможно стоит разделить обработку движения змейки в модели и обновление экрана.

## Factory (Задача 4). Ok.

**Код** - Ok.

- [Ok] Ошибка компиляции:
```
    [javac] /home/alxr/study/oop/charts/repos/baranov/java_lab4_factory.git/src/main/java/ru/nsu/g/ibaranov/factory/threadpool/ThreadPool.java:26: error: constructor Worker in class Worker cannot be applied to given types;
    [javac]             var worker = new Worker(this);
    [javac]                          ^
    [javac]   required: Storage<Body>,Storage<Motor>,Storage<Accessory>,Storage<Auto>
    [javac]   found:    ThreadPool
    [javac]   reason: actual and formal argument lists differ in length
    [javac] 1 error
```

- [Ok] ThreadPool.java: `import javafx.concurrent.Worker` не нужен.
