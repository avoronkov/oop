# Мустафин Дамир

Предварительная оценка - `5` (автомат).

Презентация. Ok.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [OK] Поправить `build.xml`, чтобы `jar` и `streamjar` собирались в разные файлы.

**Тесты** - Ok.

## Stack PL (Задача 2). OK.

**Код** - Ok.

**Тесты** - Ok.

## Tetris (Задача 3). Ok.

**Код** - Ok.

- [Ok] Сделать окно растягиваемым.

- [Ok] `Timer` должен быть частью модели, а не контроллера.

- [Ok] Добавить консольный интерфейс (?)

**Тесты** - Ok.

## Фабрика (Задача 4). Ok.

**Код** - ОК.

**Тесты** - Ok.

- [Ok] в репозитории отсутствует папка `test`.

## Чат (Зачада 5). Ok.

**Код** - Ok.

- [Ok] Добавить XML (или JSON) сериализацию сообщений.
