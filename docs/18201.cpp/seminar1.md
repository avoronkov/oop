# Семинар 1 (04.09.2019)

## Примеры кода на языке C++

Helloworld на C++:
```C++
#include <iostream>

int main() {
	std::cout << "hello world!" << std::endl;
	return 0;
}
```

Чтение из стандартного потока ввода в переменную:
```C++
#include <iostream>
#include <string>

int main() {
	// чтение целого числа
	int x;
	std::cin >> x;
	
	// Чтение слова
	std::string word;
	std::cin >> word;
}
```

Вектор в C++ ([документация](http://www.cplusplus.com/reference/vector/vector/)).
```C++
#include <iostream>
#include <vector>

using std::vector;

int main() {
	// создание пустого вектора
	vector<int> vi;

	//	добавление элементов в конец
	vi.push_back(1);
	vi.push_back(3);
	vi.push_back(5);

	vi[2] += 2;
	
	// Простая итерация по вектору
	for (int i=0; i<vi.size(); i++) {
		std::cout << vi[i] << std::endl;
	}

	// Создание вектора с заданными значениями (C++11)
	vector<int> v2{1, 2, 3, 5};
	
	// Итерация, основанная на диапазоне значений (C++11)
	for (int x: v2) {
		std::cout << x << std::endl;
	}
}
```

Чтение последовательности слов из стандартного потока ввода:
```C++
#include <iostream>
#include <vector>
#include <string>

using std::cin;
using std::vector;
using std::string;

int main() {
	vector<string> words;
	string word;
	while(cin >> word) {
		words.push_back(word);
	}
}
```

Ассоциативный массив (также известный как словарь/отображение/map)
```
#include <map>
#include <string>

int main() {
	std::map<std::string, int> m;
	m["foo"] = 2;
	m["bar"] = 4;
	m["baz"] = 7;
	for (auto x: m) {
		std::cout << m.first << " => " << m.second << std::endl;
	}
}
```
