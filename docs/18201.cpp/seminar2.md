# Семинар 2 (11.09.2019)

## Лекция

Что было на лекции.

## Современные подходы к разработке кода.

Системы контроля версий

* Git

* Gitlab. Процесс сдачи задач.

Подходы

* Разбиение программы на логические "блоки"

* DRY (Don't repeat yourself)

Автоматическое блочное тестирование кода (unit-тесты)

* catch (2)

Системы сборки кода

* Cmake

"Непрерывная интеграция" (CI, pipelines)

* Gitlab CI

## ООП

Классы, объекты.

Наследование на примере иехархии потоков (streams).
