# Задача 2. Тритсет (tritset)

[Оригинальное условие задачи](https://docs.google.com/document/d/1NM_qbZ3wJUjY_5fa807d3zlcjxzt6d0dX6MIh6Ma5eg)

## Комментарии

* Вместо библиотеки Google Test Framework рекоммендуется использовать библиотеку [Catch2](https://github.com/catchorg/Catch2)
(как в первой задаче).

* Для реализации тритов следует использовать `enum class`, например:
```
enum class Trit {False, True, Unknown}
```

* Операторы `&`, '|' и другие для тритов можно реализовать как свободные функции:
```
Trit operator&(const Trit & a, const Trit & b) { ... }
```

* Методы, не изменяющие состояние объекта должны быть помечены как `const:
```
    int length() const;
```

* Оператор взятия индекса имеет две форму: константную и неконстантную.

__Обновлено__

* Не забывать про реализацию операторов `operator &=`, `operator |=`

* _Дополнительное задание 1._ Реализовать конструктор тритсета от списка инициализации, т.е.:
```
Tritset t{Trit::True, Trit::Uknown, Trit::False};
// или 
Tritset t = {Trit::True, Trit::Uknown, Trit::False};
```
(см. std::initializer_list)

* _Дополнительное задание 2._ Реализовать операторы вывода в поток для классов Trit и Tritset.

* _Дополнительное задание 3._ Реализовать foreach-like (ranged based for loop) for для итерации по тритсету, т.е.:
```
Tritset ts{Trit::True, Trit::Unknown, Trit::False};
for (const Trit & t : ts) {
    std::cout << t << std::endl;
}
```

__Обновлено__

* В реализации оператора присваивания есть (как минимум) два нюанса:

1. Проверка на присваивание самому себе, примерно так:
```
Tritset& operator=(const Tritset& obj) {
    if (this == &obj) {
        return *this;
    }
    // присваивание
    return *this;
}
```

2. Если в объекте есть динамически выделенная память, то её нужно удалить перед присваиванием во избежание утечки памяти.
