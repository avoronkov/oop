# Решетняк Алексей

Предварительная оценка - `3`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Программа падает при запуске без аргументов.

- [Ok] Программа падает если количество слов меньше, чем длина фразы.

**Тесты** - Ok.

- [Ok] добавить тесты на разбор аргументов.

## Тритсет (Задача 2). Ok.

**Код** - Ok.

- [Ok] Методы `length` и `cardinality` должны быть константными.

- [Ok] Реализовать `operator!=` для класса `TritSet`.

- [Ok] Падает следующий тест:
```
TEST_CASE("resize", "Tritset") {                               
    TritSet a;                                                 
    
    a[9] = TRUE;                                               
    REQUIRE(a.length() == 10);                                
}
```
- [Ok] Цикл for не заканчивается после шестого (последнего) элемента
```
TEST_CASE("for-each", "Tritset") {
    TRITSET a{UNKNOWN, UNKNOWN, FALSE, FALSE, FALSE, TRUE};
    std::list<TRIT> exp{UNKNOWN, UNKNOWN, FALSE, FALSE, FALSE, TRUE};
    REQUIRE(a.length() == exp.size());
    for (auto t: a) {
        CHECK(t == exp.front());
        exp.pop_front();
    }   
}

```

- [Ok] Предупреждения компилятора:
```
src/tritset.cpp: В функции-члене «int TritSet::get_last() const»:
src/tritset.cpp:18:28: предупреждение: comparison of integer expressions of different signedness: «size_t» {aka «long unsigned int»} and «const int» [-Wsign-compare]
   18 |     for (size_t it = 0; it < size_in_trits; it++)
      |                         ~~~^~~~~~~~~~~~~~~
src/tritset.cpp: В функции-члене «Trit TritSet::get(const size_t&) const»:
src/tritset.cpp:28:18: предупреждение: comparison of integer expressions of different signedness: «const size_t» {aka «const long unsigned int»} and «int» [-Wsign-compare]
   28 |     if (position > size_in_trits - 1) return Trit::Unknown;
      |         ~~~~~~~~~^~~~~~~~~~~~~~~~~~~
src/tritset.cpp: В функции-члене «size_t TritSet::cardinality(Trit) const»:
src/tritset.cpp:262:28: предупреждение: comparison of integer expressions of different signedness: «size_t» {aka «long unsigned int»} and «int» [-Wsign-compare]
  262 |     for (size_t it = 0; it < last + 1; it++)
      |                         ~~~^~~~~~~~~~
src/tritset.cpp: В функции-члене «std::unordered_map<Trit, int> TritSet::cardinality() const»:
src/tritset.cpp:272:28: предупреждение: comparison of integer expressions of different signedness: «size_t» {aka «long unsigned int»} and «const int» [-Wsign-compare]
  272 |     for (size_t it = 0; it < size_in_trits; it++)
      |                         ~~~^~~~~~~~~~~~~~~
src/tritset.cpp: В функции «Trit operator~(Trit)»:
src/tritset.cpp:359:1: предупреждение: управление достигает конца не-void функции [-Wreturn-type]
  359 | }
      | ^
```

**Тесты** - Ok.

## Drawer (Задача 3). Ok.

**Код** - Ok.

**Тесты** - Ok.
