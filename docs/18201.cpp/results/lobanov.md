# Лобанов Иван

Предварительная оценка - `3`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Фразы должны быть упорядочены по *убыванию* частоты встречаемости.

- [Ok] Программа падает, когда длина фразы больше, чем количество слов в потоке.

**Тесты** - Ok.

- [Ok] Добавить тесты на функцию `count_phrases`.

- [Ok] В тестах лучше использовать `std::stringstream` вместо файлов - при запуске теста из другой директории файл не будет найден.

## Тритсет (Задача 2). Ok.

**Код** - Ok.

- [Ok] Добавить конструктор от `std::initializer_list`

- [Ok] Добавить операторы `==` и `!=`

- [Ok] Добавить второй метод `cardinality`.

- [Ok] `Tritset& Tritset::operator!()` должен быть константным.

- [Ok] `Tritset::cardinality()` должен быть константным.

- [Ok] Добавить итератор для класса `Tritset`, методы `begin`, `end`.

- [Ok] Операторы `==` и `!=` должны быть константными.

**Тесты** - Ok.


## Drawer (Задача 3). Ok.

**Код** - Ok.

- [Ok] Поправить запуск тестов на сервере.

- [Ok] выход за границы строки при работе с std::string.
```
/builddir/build/BUILD/gcc-9.2.1-20190827/obj-x86_64-redhat-linux/x86_64-redhat-linux/libstdc++-v3/include/bits/basic_string.h:1067: std::__cxx11::basic_string<_CharT, _Traits, _Alloc>::reference std::__cxx11::basic_string<_CharT, _Traits, _Alloc>::operator[](std::__cxx11::basic_string<_CharT, _Traits, _Alloc>::size_type) [with _CharT = char; _Traits = std::char_traits<char>; _Alloc = std::allocator<char>; std::__cxx11::basic_string<_CharT, _Traits, _Alloc>::reference = char&; std::__cxx11::basic_string<_CharT, _Traits, _Alloc>::size_type = long unsigned int]: Assertion '__pos <= size()' failed.
``` 

- [Ok] Вернуть примеры обратно из `cmake-build-debug/examples/` в `examples/`

**Тесты** - Ok.
