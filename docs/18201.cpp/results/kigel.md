# Кигель Мария

Предварительная оценка - `3`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Добавить "стражи включения" ("include guards") в заголовочный файл `count_p.h`, например:
```C++
#pragma once
```

- [Ok] Программа должна печатать результат работы на `std::cout`.

- [Ok] Неправильный разбор аргументов, например при запуске с `-n 3`.

- [Ok] Использовать функцию `rasbor` внутри `main`

- [Ok] пояснение: правильный способ написать тесты на функцию `rasbor` - вынести объявление функции в заголовочный файл
(например, `count_p.h`), а определение функции разместить в одном и cpp-файлов (например, `count_p.cpp`),
тогда функцию можно будет использовать и в `main.cpp`, и в тестах.

- [Ok] Если количество слов меньше длины фразы, то результатом работы должены быть пустой вывод.

**Тесты** - Ok.

- [Ok] Добавить тесты на функцию `count_p`.

- [Ok] Добавить тесты на разбор аргументов.


## Тритсет (Задача 2). Ok.

**Код** - Ok.

- [Ok] добавить конструктор `Tritset(std::initializer_list<Trit>)`

- [Ok] Операторы `&`, `|`, `!`, `==`, `!=` должны быть константными (и принимать константную ссылку на Tritset)

- [Ok] Операторы `==`, `!=` должны принимать константную ссылку на Tritset.

- [Ok] `class Tritset has no member named 'cardinality'`
**Upd** добавить метод `cardinality` без аргументов.

- [Ok] Оба метода `cardinality` должны быть константными.

- [Ok] Неправильная работа с памятью (выделение через `new`, освобождение через `free`).
Лучше всего использовать `new unsigned int[size]`, `delete[]`.

- [Ok] Метод `TritsetProxy::operator==(const Trit&)` должен быть константным.

- [Ok] `Iterator::operator*(...)` должен возвращать `Trit` вместо `int`.
**Upd** Оператор разыменования (`operator*`) не должен принимать аргументов.

- [Ok] Добавить конструктор копирования в класс Tritset

- [Ok] Падает тест:
```
#define TRUE Trit::T
#define FALSE Trit::F
#define UNKNOWN Trit::U

TEST_CASE("basics", "Tritset") {
    const Tritset a{UNKNOWN, UNKNOWN, UNKNOWN, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE};
    const Tritset b{UNKNOWN, TRUE, FALSE, UNKNOWN, TRUE, FALSE, UNKNOWN, TRUE, FALSE};

    const Tritset aandb{UNKNOWN, UNKNOWN, FALSE, UNKNOWN, TRUE, FALSE, FALSE, FALSE, FALSE};
    const Tritset aorb{UNKNOWN, TRUE, UNKNOWN, TRUE, TRUE, TRUE, UNKNOWN, TRUE, FALSE};
    const Tritset nota{UNKNOWN, UNKNOWN, UNKNOWN, FALSE, FALSE, FALSE, TRUE, TRUE, TRUE};

    CHECK((a & b) == aandb);
    CHECK((a | b) == aorb);
    CHECK((NOT a) == nota);

    Tritset a1 = a;
    a1 &= b;
    CHECK(a1 == aandb);

    TRITSET a2 = a;
    a2 |= b;
    CHECK(a2 == aorb);
}
```

- [Ok] Тест
```
TEST_CASE("for-each", "Tritset") {
    TRITSET a{UNKNOWN, UNKNOWN, FALSE, FALSE, FALSE, TRUE};
    std::list<TRIT> exp{UNKNOWN, UNKNOWN, FALSE, FALSE, FALSE, TRUE};
    int l = exp.size();
    REQUIRE(a.length() == l); 

    int i = 0;
    for (auto t: a) {
        REQUIRE (i < l); 
        INFO("Check position " << i); 
        CHECK(t == exp.front());
        exp.pop_front();
        i++;
    }   
}
```

**Тесты** - Ok.

## Drawer (Задача 3). Ok.

**Код** - Ok.

- [Ok] Попробовать workaround [link](https://stackoverflow.com/questions/43294488/mingw-g-multiple-definition-of-vsnprintf-when-using-to-string)
```
// Characteristic.h
#define __USE_MINGW_ANSI_STDIO 0
```

**Тесты** - Ok.
