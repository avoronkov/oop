# Кононов Артем

Предварительная оценка - `5`.

## Подсчет фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Унифицировать работу с файлом и работу со стандартным потоком ввода.

- [Ok] Фразы должны выводиться в порядке _убывания_ частоты.

- [Ok] Вместо `strcmp` лучше использовать сравнение объектов класса `std::string`

- [Ok] Неправильный вывод в случае, когда число слов меньше, чем длина фразы (ожидается пустой вывод).

**Тесты** - Ok.

- [Ok] добавить тесты.

- [Ok] добавить запуск тестов в Gitlab CI.

## Тритсет (Задача 2). Ok.

**Код** - Ok.

- [Ok] Метод получения логической длины должен называться `length` вместо `get_length`.

- [Ok] `operator[]` в классе `Tritset` должен возвращать `Trit` (или специальный класс для модификации трита).

- [Ok] `operator!` в классе `Tritset` должен быть константным, и возвращать `Tritset`

- [Ok] Метод `std::unordered_map<Trit, int> cardinality()` должен быть константным.

- [Ok] Оператор приведения класса `TritsetProxy` к `Trit` должен быть константым, и не должен быть `explicit`.

- [Ok] Отсутствует конструктор копирования в классе `Tritset`.

- [Ok] Неправильно работают конструктор копирования и/или оператор сравнения: падает следующий тест:
```
	const Tritset a{Trit::U, Trit::U, Trit::U, Trit::T, Trit::T, Trit::T, Trit::F, Trit::F, Trit::F};
    const Tritset b{Trit::U, Trit::T, Trit::F, Trit::U, Trit::T, Trit::F, Trit::U, Trit::T, Trit::F};
    const Tritset aandb{Trit::U, Trit::U, Trit::F, Trit::U, Trit::T, Trit::F, Trit::F, Trit::F, Trit::F};
    
    Tritset a1 = a;
    a1 &= b;
    CHECK(a1 == aandb);
```

**Тесты** - Ok.

## Drawer (Задача 3). Ok.

**Код** - Ok.

- (Пожелание). (В `main.cpp`) сообщения об ошибках лучше писать на стандарный поток ошибок (`std::cerr`)
и звершать программу с ненулевым кодом возврата.

- [Ok] В функции `bitmap_image drawer(bitmap_image img, ...)` лучше передавать `bitmap_image` по ссылке.
Это избавит от создания лишних копий объекта `img` и от необходимости возвращать `bitmap_image` из функции.

- [Ok]  Агрументы-вектора функции `drawer` следует передавать по константной ссылке.

**Тесты** - Ok.

## Battleship (Задача 4). Ok.

**Код** - Ok.

- [Ok]
```
cpp_task4.git/Bots/Bot_Arrange.cpp:33:24: ошибка: нет декларации «INT_MAX» в этой области видимости
   33 |         int min_area = INT_MAX;
      |                        ^~~~~~~
cpp_task4.git/Bots/Bot_Arrange.cpp:2:1: замечание: «INT_MAX» is defined in header «<climits>»; did you forget to «#include <climits>»?
    1 | #include "Bot_Arrange.h"
  +++ |+#include <climits>
    2 |
```

- [Ok]
```
/home/alxr/study/oop/charts/repos/kononov/cpp_task4.git/Bots/Bot_Arrange.cpp:73:92: ошибка: нет соответствующей функции для вызова «find(std::vector<std::pair<int, int> >::iterator, std::vector<std::pair<int, int> >::iterator, std::pair<int, int>)»
   73 |     auto start = std::find(available_corns.begin(), available_corns.end(), temp.get_start());
      |
```

## Лабиринт (Задача 5). Ok.

**Код** - Ok.

**Тесты** - Ok.
