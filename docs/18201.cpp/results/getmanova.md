# Гетманова Анастасия

Предварительная оценка - `3`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] поправить CMakeLists.txt
```
add_executable(phrases main.cpp phrases.cpp)
add_executable(unittest phrases.cpp test.cpp)
```

- [Ok] ~Добавить файл `.gitlab-ci.yml` из проекта helloworld.~
Переименовать `gitlab-ci.yml` в `.gitlab-ci.yml`. Проверить, что на gitlab.com в проекте запускаются тесты.

- [Ok] Программа неправильно работает при запуске без аргументов. (Ожидается работа со стандартным потоком ввода).

**Тесты** - Ok.

- [Ok] тесты падают.

## Тритсет (Задача 2). Ok.

**Код** - Ok.

- [Ok] Методы `cardinality` должны быть константными.

- [Ok] Класс ProxyTritSet должен реализовать `operator Trit() const`

**Тесты** - в процессе.

- [Ok] Падает следующий тест:
```
#define TRUE Trit::T
#define FALSE Trit::F
#define UNKNOWN Trit::U
#define TRITSET TritSet

TEST_CASE("basics1", "Tritset") {
    const TRITSET a{UNKNOWN, UNKNOWN, UNKNOWN, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE};
    const TRITSET b{UNKNOWN, TRUE, FALSE, UNKNOWN, TRUE, FALSE, UNKNOWN, TRUE, FALSE};

    const TRITSET aandb{UNKNOWN, UNKNOWN, FALSE, UNKNOWN, TRUE, FALSE, FALSE, FALSE, FALSE};

    CHECK((a & b) == aandb);
}
```

- [Ok] Падает тест
```
TEST_CASE("resize", "Tritset") {
    TRITSET a;
    a[9] = TRUE;
    REQUIRE(ra.length() == 10);
}
```

- [Ok] Тест
```
TEST_CASE("for-each", "Tritset") {
    TRITSET a{UNKNOWN, UNKNOWN, FALSE, FALSE, FALSE, TRUE};
    std::list<TRIT> exp{UNKNOWN, UNKNOWN, FALSE, FALSE, FALSE, TRUE};
    int l = exp.size();
    REQUIRE(a.length() == l); 

    int i = 0;
    for (auto t: a) {
        REQUIRE (i < l); 
        INFO("Check position " << i); 
        CHECK(t == exp.front());
        exp.pop_front();
        i++;
    }   
}
```

## Drawer (Задача 3). Ok.

**Код** - Ok.

- [Ok] Метод `Named::generateName(const string &)` лучше сделать конструктором класса Named (`Named(const string&)`),
затем вызывать этот конструктор из конструкторов наследуемых классов.

- [Ok] Добавить примеры описаний изображений в папку `examples`.

**Тесты** - Ok.

- [Ok] Добавить тесты на методы `printNames` и `totalArea`.
