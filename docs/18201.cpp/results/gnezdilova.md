# Гнездилова Анна

Предварительная оценка - `3`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] неправильный вывод, если количество слов меньше длины фразы
(должен быть пустой вывод).

**Тесты** - Ok.

## Тритсет (Задача 2). Ok.

**Код** - OK.

- [Ok] Ошибка компиляции:
```
tritset.h:52:14: error: ‘ssize_t’ was not declared in this scope
         for (ssize_t i = length - 1; i >= 0 /* biggest length */; --i)
              ^~~~~~~
```

- [Ok] Добавить `.gitlab-ci.yml`

- [Ok] Нет оператора `!` для класса `Trit`.

- [Ok] "const class Tritset" не содержит метода с именем "length"

- [Ok] Нет оператора `operator!=` в классе Tritset.

- [Ok] Операторы `==`, `!=` должны быть константными.

- [Ok] Нет метода `cardinality()` без аргументов в классе Tritset.

- [Ok] Методы `cardinality` должны быть константными.

**Тесты** - Ok.

- [Ok] Падает следующий тест:
```
#define TRUE Trit::True
#define FALSE Trit::False
#define UNKNOWN Trit::Unknown
#define TRITSET Tritset

TEST_CASE("basics2", "Tritset") {
    const TRITSET a{UNKNOWN, UNKNOWN, UNKNOWN, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE};
    const TRITSET nota{UNKNOWN, UNKNOWN, UNKNOWN, FALSE, FALSE, FALSE, TRUE, TRUE, TRUE};
    CHECK((!a) == nota);
}
```

## Drawer (Задача 3). Ok.

**Код** - Ok.

- [Ok] Variadic шаблоны должны работать без inline. Примерно так:
```
-template <typename... Args>
-void printNames(Named const & named, Args... args)
+template <typename T, typename... Args>
+void printNames(T const & named, Args... args)

-inline void printNames(Named const & named)
+template<typename T> void printNames(T const & named)
```

**Тесты** - Ok.

- [Ok] Добавить тесты на методы `printNames` и `totalArea`
