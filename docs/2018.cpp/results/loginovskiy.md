# Логиновский Степан (недопуск)

Предварительная оценка - `-`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok

- [Ok] Нет доступа.
Следует сменить роль преподавателя с `Guest` на `Developer` в группе `17201_oop_loginovskiy`.
([инструкция](/gitlab/))

- [Ok] Аргументы-объекты при передаче в функции следует передавать по (константной) ссылке:
```
-std::map<std::string, int> makephmap(std::vector<std::string> phrases);
+std::map<std::string, int> makephmap(const std::vector<std::string>& phrases);
```

- [Ok] `Если имя файла не указано, то программа также работает со стандартным потоком ввода.`

**Тесты** - Ok.

- [Ok] `catch.hpp`, `.gitlab-ci.yml` потерялся.

- [Ok] В тестах нужно использовать `std::stringstream`
