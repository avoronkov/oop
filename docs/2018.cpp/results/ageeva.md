# Агеева Анастасия (3)

Предварительная оценка - `3`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Фразы в выводе должны быть упорядочены по убыванию частоты, а не возрастанию
(самые часто встречающиеся - вначале)

- [Ok] Объекты (например, `map`, `vector` и т.д.) следует передавать в методы/функции по константной ссылке,
вместо передачи по значению во избежание лишнего копирования.
(например, `std::map<string, int> makePhrases(const std::vector<string>&, int);`)
**upd** не забывать про ключевое слово `const`.

- [Ok] Запрещено использовать `using` в заголовочных файлах ([ссылка](/using-namespaces/))

- [Ok] Что будет если во входном потоке слов меньше, чем длина фразы?
**upd:** тест всё ещё падает.

- [Ok] **upd:** причина предыдущего падения: состояние потока нужно проверять после чтения, а не до него.
(В противном случае будет лишний раз считано последнее слово, если в конце потока были пустые символы).
Т.е.
```
// Вместо
	while (in.good())
	{
		in >> tmp;
// должно быть
	while (in >> tmp)
	{
```
(operator>> возвращает ссылку на поток, которые в булевом контексте проверяет `good()`).

- [Ok] Job failed.
Вообще, изначально предполагалось, что на каждую лабораторную будет отдельный репозиторий,
но в принципе допустимо хранить все лабы в одном репозитории
(хотя, возможно это будет создавать небольшие трудности со следующими проектами).

В любом случае, если оставлять текущую структуру, то следует исправить файл `.gitlab-ci.yml` примерно так:
```
...
script: 
    - mkdir -p lab1/build/
    - cd lab1/build/
    - cmake ..
    - make
    - ./unittest -s
```

- [Ok] Также в `CMakeLists.txt` следует поставить минимальную версию поменьше, например `cmake_minimum_required(VERSION 3.2)`.
И поправить имя файла `test.cpp` на `tests.cpp`.

- [Ok] Потерялся файл `.gitlab-ci.yml` для запуска тестов из исходного примера: https://gitlab.com/avoronkov/helloworld/blob/master/.gitlab-ci.yml

- [Ok] Если указан аргумент `-`, то программа выводит только фразу `Enter text:`.
**upd:** Фраза `Enter text:` не нужна.

- [Ok] Если файл не указан, то программа не считывает данные со стандартного потока ввода.

**Тесты** - Ok.

- [Ok] Тесты падают (обновлено):
```
phrases-counter.git/tests.cpp:16: FAILED:
  REQUIRE( funcRes == testVec )
with expansion:
  { "" } == { "the", "lazy", "dog" }

-------------------------------------------------------------------------------
makePhrases
-------------------------------------------------------------------------------
/home/alxr/study/oop/charts/tmp.repos/ageeva/phrases-counter.git/tests.cpp:19
...............................................................................

phrases-counter.git/tests.cpp:29: FAILED:
  REQUIRE( funcRes == testMap )
with expansion:
  {  } == { {?}, {?}, {?} }

===============================================================================
test cases: 3 | 1 passed | 2 failed
assertions: 3 | 1 passed | 2 failed
```

## Тритсет (Задача 2). Ok.

**Код** - Ok.

- [Ok] Добавить `CMakeLists.txt` и `.gitlab-ci.yml`
**Upd** переименовать `gitlab-ci.yml` в `.gitlab-ci.yml`.

- [Ok] Не забывайте про `const`:

1. [Ok] Методы, котоыре не изменяют состояние объекта должны быть помечены как const:
```
- size_t length();
+ size_t length() const;
```

2. [OK] Неизменяемые аргументы должны передаваться как константные ссылки:
```
-Tritset operator & (Tritset& a);
+Tritset operator & (const Tritset& a) const;
```

- [Ok] **Upd:** В возвращаемом значении `const` не нужен:
```
-const Trit operator & (const Trit& a, const Trit& b)
+Trit operator & (const Trit& a, const Trit& b)
```

- [Ok] `gitlab.yml` -> `.gitlab.yml`

- [Пожелание]. Предупреждения компилятора:
```
/home/alxr/study/oop/charts/tmp.repos/ageeva/tritset.git/Tritset.cpp:90:1: warning: control reaches end of non-void function [-Wreturn-type]
/home/alxr/study/oop/charts/tmp.repos/ageeva/tritset.git/Trit.cpp:11:1: warning: control reaches end of non-void function [-Wreturn-type]
/home/alxr/study/oop/charts/tmp.repos/ageeva/tritset.git/Trit.cpp:23:1: warning: control reaches end of non-void function [-Wreturn-type]
/home/alxr/study/oop/charts/tmp.repos/ageeva/tritset.git/Trit.cpp:33:1: warning: control reaches end of non-void function [-Wreturn-type]
/home/alxr/study/oop/charts/tmp.repos/ageeva/tritset.git/reference.cpp:17:1: warning: control reaches end of non-void function [-Wreturn-type]
/home/alxr/study/oop/charts/tmp.repos/ageeva/tritset.git/reference.cpp:47:1: warning: control reaches end of non-void function [-Wreturn-type]
/home/alxr/study/oop/charts/tmp.repos/ageeva/tritset.git/iterator.cpp:40:1: warning: control reaches end of non-void function [-Wreturn-type]
```

**Тесты** - Ok

- [Ok] Добавить сборку юниттестов в `CMakeLists.txt`

## Drawer (Задача 5). Ok.

**Код** - Ok.

- [Ok] реализовать класс `Colored`.

- [Ok] реализовать методы `printNames`... 

- [Ok] Ошибка компиляции (похоже, нужен метод `at` вместо `operator[]`, т.к. у последнего нет константного варианта).
```
drawer.git/Header.h:58:40: error: passing ‘const std::map<std::__cxx11::basic_string<char>, Color>’ as ‘this’ argument discards qualifiers [-fpermissive]
         shapeColor = colorSet[colorName];
                                        ^
```

**Тесты** - Ok.

- [Ok] Добавить тесты (пустой `tests.cpp`)
