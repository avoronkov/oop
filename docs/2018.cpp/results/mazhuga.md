# Мажуга Дмитрий (5)

Предварительная оценка - `5`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] В функции `get_parameters` в случае ошибки следует писать сообщение на стандартный поток ошибок (`std::cerr`),
и выходить c ненулевым кодом возврата (например `exit(1);`).

- [Ok] То же самое в `main`

- [Ok] Фразы в выводе должны быть отсортированы в обратном порядке
("в порядке убывания частоты встречаемости").

**Тесты** - Ok.

- [Ok] `Segmentation fault      (core dumped) ./unittest -s` - ошибка при работе с памятью в третьем тесте.
(не выделена память для 6 элкментов argv)

- [Ok] Вместо использования файлов в тестах, лучше передавать в `count_phrases` `std::stringstream` с содержимым файлов.

- [Ok] Удалить память, выдеденную через `new[]` через `delete[]`

## Тритсет (Задача 2). Ok.

**Код** - Ok.

- [Ok] Выполнить [дополнительные задания](/2018.cpp/task2).

**Тесты** - Ok.

## Резолвер (Задача 3). Ok.

**Код** - Ok.

- [Ok] `main.cpp`:
```
    bs.open("tests/07libs.txt");
    targets.open("tests/07targets.txt");
```

**Тесты** - Ok.

- [Ok] Все тесты упали (скорее всего из-за чтения из `aaa/06libs.txt` и `aaa/06targets.txt`)

## Морской бой (Задача 4). Ok.

**Код** - Ok.

**Игра** - Ok.

## Drawer (Задача 5). Ok.

**Код** - Ok.

- [Ok] В методе `void drawShape(...)` следует принимать аргумент по константной ссылке.

- [Ok] Все классы имеющие наследников должны иметь виртуальный деструктор.

- [Пожелание] Подозрительный `_GLIBCXX_USE_NOEXCEPT` в классе `DrawerException`.
Кажется, вместо него должен быть просто `noexcept`.
Вообще, можно сделать ещё проще, если наследоваться от класса с конструктором от строки и готовым методом `what`:
```
class DrawerException : public std::logic_error {
public:
    DrawerException(const std::string & msg): std::logic_error(msg) { }
};
```

**Тесты** - Ok.

- [Ok] Падает тест:
```
munmap_chunk(): invalid pointer
/home/alxr/study/oop/charts/tmp.repos/mazhuga/task5_drawer.git/test.cpp:101: FAILED:
  {Unknown expression after the reported line}
with expansion:

due to a fatal error condition:
  SIGABRT - Abort (abnormal termination) signal
```
