# Берьянов Максим (5)

Предварительная оценка - `5`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Использование `using namespace std;` в заголовочном файле `functions.h` ([подробнее](/using-namespaces/#2))
__Upd:__ также не следует использовать `using std::...;` в заголовочных файлах.

- [Ok] Код заголовочного файла `functions.h` должен или целиком располагаться внутри "стражей включения" (include guards) `#ifndef UNTITLED1_FUNCTIONS_H ... #endif`,
или вместо них можно использовать `#pragma once`

- [Fine] В `make_phrases()` вместо `new string[amount]` следует использовать `std::vector`.
Использование оператора `new` без оператора `delete` приводит к утечкам памяти.

- [Ok] Вместо `while (!feof(stdin))` следует проверять std::cin (поскольку чтение осуществляется через него),
например `while (std::cin)`

- [Ok] Удалить ненужные файлы `hello.h`, `hello.cpp` (`git rm hello.h hello.cpp`)

- [Ok] "Если имя файла не указано, то программа также работает со стандартным потоком ввода."

**Тесты** - Ok.

- [Ok] Добавить тесты на функцию `make_phrases`

## Тритсет (Задача 2). Ok.

**Код** - Ok.

- [Ok] Изменения в репозитории должны осуществляться через добавление новых коммитов,
а не через создание нового репозитория.
([git-туториал](https://git-scm.com/book/ru/v1/Основы-Git), ключевые команды `git add`, `git commit`, `git push`, `git pull`)

- [Ok] "Странная" семантика оператора взятия индекса (`operator[]`).
Этот оператор должен возвращать элемент коллекции (Trit) или вспопогательный класс, позволяющий изменять элемент в коллекции (т.н. TritProxy).
Т.е.:
```
class tritset {
...
    Trit operator[](int i) const;
    TritProxy operator[](int i);
...
};
```
В данной реализации не работает следующий тест:
```
TEST_CASE("assignment") {
    tritset a;
    a[10] = Trit::True;
    REQUIRE(a[10] == Trit::True);
}
```
но позволяются конструкции вроде этой:
```
    tritset a;
    a = Trit::True;
```

- [OK] В опереаторе присваивания (`operator=`) перед выделением памяти следует удалять ранее выделенную память (иначе - memory leak).

- [Ok] Оператор `&=` следует реализовывать через оператор `&` и оператор присваивания (!) (можно наоборот - `&` реализовать через `&=` и копирование).

**Тесты** - Ок.

## Резолвер (Задача 3). Ok.

**Код** - Ok.

- [Ok] Вместо `freopen` в `main.cpp` следует использовать `std::ifstream`.

- [Ok] Но вместо `...; std::cin.rdbuf(in.rdbuf()); ... filling_every_libs_up(std::cin, every_libs);` можно написать проще:
```
std::ifstream in("libs.txt");
filling_every_libs_up(std::cin, every_libs);
```

**Тесты** - Ok.

- [Ok] Не проходит тест из [примера 6](https://avoronkov.gitlab.io/oop/2018.cpp/task3/#_8)

- [Ok] Поправить сообщение об ошибке: `Unable to resolve targets.` вместо `unable to resolve the targets`.

## Морской бой (Задача 4). Ok.

**Код** - Ok.

**Игра** - Ok.

## Drawer (Задача 5). Ok.

**Код** - Ok.

- [Ok] В классе `Named` не должно храниться информации о классах, которые его используют (`rectangle_amount`, `triangle_amount`).
В противном случае, невозможно добавлять новые классы без модификации `Named`.
Для подсчета индекса в имени элемента нужно использовать `std::map<std::string, int>`, где ключ - имя фигуры (передается самой фигурой).

- [Ok] Также функция подсчета площади должна быть реализована в подклассах `Circle`, `Rectangle`...

- [Ok] Метод `getSegments` должен возвращать вектов отрезков, которые потом будут рисоваться внешним Drawer'ом.
Примерно так:
```
	class Segment{...};
	std::vector<Segment> getSegments() const;
```

- [Ok] У функции `findCanvas` следует поменять сигнатуру, чтобы она возвращала полученные значения, а не записывала их по ссылке.
Например:
```
std::pair<int, int> find_canvas(std::istream& in);
```

- [Ok] В функции `observing_shapes` следует разделить парсинг строк и отрисовку фигур.
Для этого следует реализовать две функции:
```
// Чтение строки и создание фигуры.
std::shared_ptr<Shape> parseShape(const std::string & line);

// Рисование фигуры
void drawShape(std::shared_ptr<Shape> shape);
```
Функция `parseShape` создает фигуру нужного типа и возвращает "умный" указатель на нее.
Функция `drawShape` ничего не знает о конкретных фигурах и рисует их используя методы `getColor` из класса `Colored`
и `getSegments` из класса `Shape`.

- [Ok] Поля `int red_color;`, `int green_color;`, `int blue_color;`
должны быть реализованы в базовом классе `Colored`, а не в каждом классе наследнике.

- [Ok] Соответсвующие методы `Color getColor() const` также должны реализованы в классе `Colored`.

- [Ok] Класс Drawer не должен наследоваться от Shape.

- [Ok] В функции main() сделать чтение из стандартного потока ввода.

**Тесты** - Ok.

- [Ok] Похоже на неправильную работу с памятью.
```
/builddir/build/BUILD/gcc-8.1.1-20180712/obj-x86_64-redhat-linux/x86_64-redhat-linux/libstdc++-v3/include/bits/basic_string.h:1058: std::__cxx11::basic_string<_CharT, _Traits, _Alloc>::reference std::__cxx11::basic_string<_CharT, _Traits, _Alloc>::operator[](std::__cxx11::basic_string<_CharT, _Traits, _Alloc>::size_type) [with _CharT = char; _Traits = std::char_traits<char>; _Alloc = std::allocator<char>; std::__cxx11::basic_string<_CharT, _Traits, _Alloc>::reference = char&; std::__cxx11::basic_string<_CharT, _Traits, _Alloc>::size_type = long unsigned int]: Assertion '__pos <= size()' failed.
```
