# Масеевский Антон (4)

Предварительная оценка - `4`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Передача аргументов-объектов должна осуществляться по ссылке.
```
-std::vector<std::string> makephrases(std::vector<std::string> words, int n);
+std::vector<std::string> makephrases(const std::vector<std::string> &words, int n);
```
**Upd:** не зыбывать про `const`.

- [Ok] `со стандартным потоком ввода, если в качестве имени файла указан -`

- [Ok] `Если имя файла не указано, то программа также работает со стандартным потоком ввода`

- [Ok] падает при запуске:
```
/home/alxr/study/2018.cpp/maseevskiy/task1/build/phrasecount -n 3 - < ./testdata/input02.txt
./tester.sh: line 19:  6199 Segmentation fault      (core dumped) /home/alxr/study/2018.cpp/maseevskiy/task1/build/phrasecount -n 3 - < ./testdata/input02.txt
[!] test 3 failed: exit code = 139
```
с такими данными:
```
one two three four one two three five one two three four
```

- [Ok] Сообщения об ошибках следует писать в `std::cerr` вместо `std::cout`.

- [Ok] Если в качестве аргументов указан только 1 файл, то программа падает:
```
6. read file
/home/alxr/study/oop/charts/tmp.repos/maseevskiy/task1.git/build/phrasecount ./testdata/input01.txt
[!] test 6 failed: exit code = 3
Output was:  Cannot make phrase with less words than it's length should be
```

- [Ok] В случае, если длина фразы больше количества слов в вводе, то программа должна корректно завершаться с кодом возврата 0 и пустым выводом.

**Тесты** - Ok.

- [Ok] Вместо 
```
    words.push_back("This");
    words.push_back("is");
    words.push_back("a");
    words.push_back("simple");
```
можно написать
```
vector<std::string> words = {"This", "is", "a", "simple", ...};
```

## Тритсет (Задача 2). Ok.

**Код** - Ok.

- [Ok] Использовать `enum class` вместо `enum`.

- [Ok] `operator&`, `operator&=` должны принимать аргумент по константной ссылке (`const Tritset &`)

- [Ok] добавить итератор, методы `begin()`, `end()` для реализации range-based for-loop.

- [Ok] Конструктор от списка инициализации должен работать с Trit'ами:
```
-TritSet (std::initializer_list<TritRef> list);
+TritSet (std::initializer_list<Trit> list);
```

**Тесты** - Ok.

## Резолвер (Задача 3). Ok.

**Код** - Ok.

- [Ok] Вывод программы должен осуществляться на стандартный поток вывода (`std::cout`)

- [Ok] `В случае, если невозможно разрешить зависимости ... программа завершается с кодом возврата 1.`

**Тесты** - Ok.

- [Ok] Добавить тесты.

## Морской бой (Задача 4). Ok.

**Код** - Ok.

**Игра** - Ok.
