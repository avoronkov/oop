# Щеголева Ангелина (4)

Предварительная оценка - `4`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Функция `print_result` должна принимать аргумент `phrases` как константную ссылку
(`const std::vector<std::pair<std::string, int>> &phrases`).

- [Ok] Сообщения об ошибках (`"Bad input"`) лучше писать в стандартный поток ошибок (`std::cerr`).

- [Fine] В функцию `check_input` логично добавить ещё один аргумент: `std::string &filename`.
Тогда имя файла будет обрабатываться также как остальные аргументы.

- [Ok] Вместо `strcmp` следует использовать сравнение плюсовых строк (напр. `std::string(argv[argc-1]) == "-"`).

**Тесты** - Ok.

- [Ok] Память выделенную через `new` следует удалять через `delete`. (Для `new[]` соответсвенно - `delete[]`).

## Тритсет (Задача 2). Ok.

**Код** - Ok.

- [Ok] Реализовать итерацию по тритсету через `begin()`, `end()` и итераторы.

**Тесты** - Ok.

- [Ok] Добавить тест на range-based for.

## Резолвер (Задача 3). В процессе.

**Код** - в процессе.

- Опечатка:
```
    freopen("targetss.txt", "r", stdin);
```

- Вместо `freopen` следует использовать файловые потоки.

**Тесты** - в процессе.

- Падают:
```
--- temp.output.txt     2019-01-19 14:08:59.630141849 +0700
+++ ./testdata/output01.txt     2018-11-18 19:40:30.375184897 +0700
@@ -0,0 +1 @@
+A 2.0
[!] test 1 failed: incorrect output
--- temp.output.txt     2019-01-19 14:08:59.642140303 +0700
+++ ./testdata/output02.txt     2018-11-18 19:40:30.375184897 +0700
@@ -0,0 +1,2 @@
+A 1.0
+B 2.0
[!] test 2 failed: incorrect output
[!] test 3 failed: incorrect exit code: actual 134, expected 0
--- temp.output.txt     2019-01-19 14:08:59.758125367 +0700
+++ ./testdata/output04.txt     2018-11-18 19:40:30.375184897 +0700
@@ -0,0 +1 @@
+A 1.5
[!] test 4 failed: incorrect output
[!] test 5 failed: incorrect exit code: actual 139, expected 0
[!] test 6 failed: incorrect exit code: actual 134, expected 0
[!] test 7 failed: incorrect exit code: actual 134, expected 1
7 tests failed
```

## Морской бой (Задача 4). Ok.

**Код** - Ok.

**Игра** - Ok.

## Drawer (Задача 5). Ok.

**Код** - Ok.

- [Ok] Все классы с наследниками должны иметь виртуальный деструктор.

- [Ok] Пожелание. отдельный класс `Segments` не нужен, абстрактный метод `getSegments` можно объявить в классе `Shape`.

- [Пожелание] разбить `Header.h` на отдельные `.h` и `.cpp` файлы.

**Тесты** - Ok.
