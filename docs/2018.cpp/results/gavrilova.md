# Гаврилова Дарья (4)

Предварительная оценка - `4`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] В функциях лучше возвращать результат, а не принимать ссылку на результат в качестве аргумента.
Т.е. вместо
```
void makeWords(std::istream &, std::vector<std::string> &);
```
лучше написать:
```
std::vector<std::string> makeWorkds(std::istream &);
```

- [Ok] Не следует использовать `using` в заголовочных файлах.

**Тесты** - Ok.

- [Ok] Добавить тесты

- (Функциональные тесты пройдены)

## Тритсет (Задача 2). Ok.

**Код** - Ok.

- [Ok] Оператор вывода в поток должен принимать Tritset по константной ссылке:
```
std::ostream &operator<<(std::ostream &out, const Tritset &set);
```

- [OK] Метод `cardinality` должен быть константным:
```
size_t cardinality(Trit);
```

**Тесты** - Ok.

## Резолвер (Задача 3). Ok.

**Код** - Ok.

**Тесты** - Ok.

## Drawer (Задача 5). Ok.

**Код** - Ok.

- [Ok] Инверсия зависимостей.
Класс `Shape` не должен ничего знать о `bitmap` и способах отрисовки на холсте.
Т.е. метод `drawShape` должен находиться в классе `Drawer`, а не в классе `Shape`
(т.к. именно класс `Drawer` отвечает за отрисовку фигур и знает и про фигуры и про холст).

- [Ok] Метод `Colored::getColor()` должен быть константным.

**Тесты** - Ok.

- [Ok] Добавть тест, который рисует некоторую картинку.
