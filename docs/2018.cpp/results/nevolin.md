# Неволин Владимир (5)

Предварительная оценка - `4`.

## Подсчёт фраз (Задача 1). Ok.

**Код** - Ok.

- [Ok] Для сортировки нужно использовать `std::sort`.

- [Ok] Для передачи аргументов по ссылке следует использовать ссылки вместо указателей.
```
// Вместо 
void SortPhrases(std::vector<Phrase>* Text_Phrases);
// должно быть 
void SortPhrases(std::vector<Phrase>& Text_Phrases);
```

- [Ok] Неправильная передача константной ссылки:
```
-void Phrase::SetData(const std::string NewData) {
+void Phrase::SetData(const std::string &NewData) {
```

- [Ok] Вместо
```
- while(InputFile.good()){
-     InputFile >> last_str;
+ while(InputFile >> last_str) {
```

- [Ok] Порядок вывода - от сымых частых к самых редких.

**Тесты** - Ok.


## Резолвер (Задача 3). Ok.

**Код** - Ok.

**Тесты** - Ok.

- [Ok] Все тесты упали:
```
[!] test 1 failed: incorrect exit code: actual 139, expected 0
[!] test 2 failed: incorrect exit code: actual 139, expected 0
[!] test 3 failed: incorrect exit code: actual 139, expected 0
--- temp.output.txt     2018-12-23 16:27:45.808306169 +0700
+++ ./testdata/output04.txt     2018-11-18 19:40:30.375184897 +0700
@@ -1,14 +1 @@
-libs.txt
-A 2.0
-
 A 1.5
-
-A 1.0
-
-Targets.txt
-<<A 1.8
-
-A 2.0 A 1.5 A 1.0
-A 2.0 0 0 0
-A 1.5 0 2 0
-A 1.0 0 0 2
[!] test 4 failed: incorrect output
[!] test 5 failed: incorrect exit code: actual 139, expected 0
[!] test 6 failed: incorrect exit code: actual 139, expected 0
[!] test 7 failed: incorrect exit code: actual 139, expected 1
7 tests failed
```

## Морской бой (Задача 4). Ok.

**Код** - Ok.

**Игра** - Ok.


## Drawer (Задача 5). Ok.

**Код** - Ok.

**Тесты** - Ok.
