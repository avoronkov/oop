# Морской бой

![battle 2019.01.08](/2018.cpp/battle.2019.01.08.gif)

## Статистика

```
karavaev: wins 5 (clean: 5) times, looses 9 (tech: 0) times.
maseevskiy: wins 9 (clean: 9) times, looses 5 (tech: 0) times.
mazhuga: wins 7 (clean: 7) times, looses 7 (tech: 0) times.
nevolin: wins 2 (clean: 2) times, looses 12 (tech: 0) times.
parfenov: wins 11 (clean: 11) times, looses 3 (tech: 0) times.
pleshkov: wins 13 (clean: 13) times, looses 1 (tech: 0) times.
shchegoleva: wins 0 (clean: 0) times, looses 14 (tech: 0) times.
beryanov: wins 9 (clean: 9) times, looses 5 (tech: 0) times.

beryanov vs karavaev : winner is beryanov
beryanov vs maseevskiy : winner is beryanov
beryanov vs mazhuga : winner is beryanov
beryanov vs nevolin : winner is beryanov
beryanov vs parfenov : winner is parfenov
beryanov vs pleshkov : winner is pleshkov
beryanov vs shchegoleva : winner is beryanov
karavaev vs beryanov : winner is beryanov
karavaev vs maseevskiy : winner is maseevskiy
karavaev vs mazhuga : winner is mazhuga
karavaev vs nevolin : winner is karavaev
karavaev vs parfenov : winner is parfenov
karavaev vs pleshkov : winner is pleshkov
karavaev vs shchegoleva : winner is karavaev
maseevskiy vs beryanov : winner is maseevskiy
maseevskiy vs karavaev : winner is maseevskiy
maseevskiy vs mazhuga : winner is maseevskiy
maseevskiy vs nevolin : winner is maseevskiy
maseevskiy vs parfenov : winner is parfenov
maseevskiy vs pleshkov : winner is pleshkov
maseevskiy vs shchegoleva : winner is maseevskiy
mazhuga vs beryanov : winner is mazhuga
mazhuga vs karavaev : winner is karavaev
mazhuga vs maseevskiy : winner is maseevskiy
mazhuga vs nevolin : winner is mazhuga
mazhuga vs parfenov : winner is parfenov
mazhuga vs pleshkov : winner is mazhuga
mazhuga vs shchegoleva : winner is mazhuga
nevolin vs beryanov : winner is beryanov
nevolin vs karavaev : winner is karavaev
nevolin vs maseevskiy : winner is maseevskiy
nevolin vs mazhuga : winner is mazhuga
nevolin vs parfenov : winner is parfenov
nevolin vs pleshkov : winner is pleshkov
nevolin vs shchegoleva : winner is nevolin
parfenov vs beryanov : winner is beryanov
parfenov vs karavaev : winner is parfenov
parfenov vs maseevskiy : winner is parfenov
parfenov vs mazhuga : winner is parfenov
parfenov vs nevolin : winner is parfenov
parfenov vs pleshkov : winner is pleshkov
parfenov vs shchegoleva : winner is parfenov
pleshkov vs beryanov : winner is pleshkov
pleshkov vs karavaev : winner is pleshkov
pleshkov vs maseevskiy : winner is pleshkov
pleshkov vs mazhuga : winner is pleshkov
pleshkov vs nevolin : winner is pleshkov
pleshkov vs parfenov : winner is pleshkov
pleshkov vs shchegoleva : winner is pleshkov
shchegoleva vs beryanov : winner is beryanov
shchegoleva vs karavaev : winner is karavaev
shchegoleva vs maseevskiy : winner is maseevskiy
shchegoleva vs mazhuga : winner is mazhuga
shchegoleva vs nevolin : winner is nevolin
shchegoleva vs parfenov : winner is parfenov
shchegoleva vs pleshkov : winner is pleshkov
```

## Лог игры

[лог](/2018.cpp/game-2019.01.08.log)
