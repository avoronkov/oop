# Результаты.

## Агеева Анастасия

_(обновлено 08 янв 2019)_

Предварительная оценка - `-`.

[Подсчёт фраз - Ok](/2018.cpp/results/ageeva/#1)

[Тритсет - Ok](/2018.cpp/results/ageeva/#2)

[Drawer - В процессе](/2018.cpp/results/ageeva/#3)

## Берьянов Максим

_(обновлено 03 дек 2018)_

Предварительная оценка - `5`.

[Подсчёт фраз - Ok](/2018.cpp/results/beryanov/#1)

[Тритсет - Ok](/2018.cpp/results/beryanov/#2)

[Резолвер - Ok](/2018.cpp/results/beryanov/#3)

[Морской бой - Ok](/2018.cpp/results/beryanov/#4)

[Drawer - Ok](/2018.cpp/results/beryanov/#5)

## Гаврилова Дарья

_(обновлено 12 янв 2019)_

Предварительная оценка - `4`.

[Подсчёт фраз - Ok](/2018.cpp/results/gavrilova/#1)

[Тритсет - Ok](/2018.cpp/results/gavrilova/#2)

[Резолвер - Ok](/2018.cpp/results/gavrilova/#3)

[Drawer - Ok](/2018.cpp/results/gavrilova/#4)

## Караваев Григорий

_(обновлено 17 дек 2018)_

Предварительная оценка - `5`.

[Подсчёт фраз - Ok](/2018.cpp/results/karavaev/#1)

[Тритсет - Ok](/2018.cpp/results/karavaev/#2)

[Резолвер - Ok](/2018.cpp/results/karavaev/#3)

[Морской бой - Ok](/2018.cpp/results/karavaev/#4)

[Drawer - Ok](/2018.cpp/results/karavaev/#5)

## Логиновский Степан

_(обновлено 03 дек 2018)_

Предварительная оценка - `-`.

[Подсчёт фраз - Ok](/2018.cpp/results/loginovskiy/#1)

## Мажуга Дмитрий

_(обновлено 17 дек 2018)_

Предварительная оценка - `5`.

[Подсчёт фраз - Ok](/2018.cpp/results/mazhuga/#1)

[Тритсет - Ok](/2018.cpp/results/mazhuga/#2)

[Резолвер - Ok](/2018.cpp/results/mazhuga/#3)

[Морской бой - Ok](/2018.cpp/results/mazhuga/#4)

[Drawer - Ok](/2018.cpp/results/mazhuga/#5)

## Масеевский Антон

_(обновлено 12 янв 2019)_

Предварительная оценка - `3`.

[Подсчёт фраз - Ok](/2018.cpp/results/maseevskiy/#1)

[Тритсет - Ok](/2018.cpp/results/maseevskiy/#2)

[Резолвер - В процессе](/2018.cpp/results/maseevskiy/#3)

[Морской бой - Ok](/2018.cpp/results/maseevskiy/#4)

## Неволин Владимир

_(обновлено 28 дек 2018)_

Предварительная оценка - `4`.

[Подсчёт фраз - Ok](/2018.cpp/results/nevolin/#1)

[Резолвер - Ok](/2018.cpp/results/nevolin/#2)

[Морской бой - Ok](/2018.cpp/results/nevolin/#3)

[Drawer - Ok](/2018.cpp/results/nevolin/#4)

## Парфенов Денис

_(обновлено 28 дек 2018)_

Предварительная оценка - `5`.

[Подсчёт фраз - Ok](/2018.cpp/results/parfenov/#1)

[Тритсет - Ok](/2018.cpp/results/parfenov/#2)

[Резолвер - Ok](/2018.cpp/results/parfenov/#3)

[Морской бой - Ok](/2018.cpp/results/parfenov/#4)

[Drawer - Ok](/2018.cpp/results/parfenov/#5)

## Плешков Андрей

_(обновлено 28 дек 2018)_

Предварительная оценка - `5(+)`.

[Подсчёт фраз - Ok](/2018.cpp/results/pleshkov/#1)

[Тритсет - Ok](/2018.cpp/results/pleshkov/#2)

[Резолвер - Ok](/2018.cpp/results/pleshkov/#3)

[Морской бой - Ok](/2018.cpp/results/pleshkov/#4)

[Морской бой, сервер - Ok](/2018.cpp/results/pleshkov/#5)

[Drawer - Ok](/2018.cpp/results/pleshkov/#6)

## Щеголева Ангелина

_(обновлено 28 дек 2018)_

Предварительная оценка - `4`.

[Подсчёт фраз - Ok](/2018.cpp/results/shchegoleva/#1)

[Тритсет - Ok](/2018.cpp/results/shchegoleva/#2)

[Морской бой - Ok](/2018.cpp/results/shchegoleva/#3)

[Drawer - Ok](/2018.cpp/results/shchegoleva/#4)

## Суммарная активность

[activity charts](/charts.17201/_all)

