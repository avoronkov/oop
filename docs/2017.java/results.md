# Результаты

## Баранов Анатолий

Доклад: *Исключения +*

Тест: *15.5*

Предварительная оценка: `5`

[Подсчёт слов - ОК](/2017.java/results/baranov/)

[Фабрика - ОК](/2017.java/results/baranov/#2)

[Сапёр - ОК](/2017.java/results/baranov/#2)

[Чат - ОК](/2017.java/results/baranov/#3)

## Болдырев Егор

Доклад: *Управление ресурсами +*

Тест: *18*

[Подсчёт слов - ОК](/2017.java/results/boldyrev/)

[Фабрика - в процессе](/2017.java/results/boldyrev/#2)

## Воробьева Анжелика

Тест: *15.5*

Предварительная оценка: `4`

[Подсчёт слов - ОК](/2017.java/results/vorobyeva/)

[Тетрис - ОК](/2017.java/results/vorobyeva/#2)

[Чат - ОК](/2017.java/results/vorobyeva/#3)

## Гусев Александр

Тест: *14.5*

Предварительная оценка: `4`

[Подсчёт слов - ОК](/2017.java/results/gusev/)

[Сапёр - ОК](/2017.java/results/gusev/#2)

[Фабрика - ОК](/2017.java/results/gusev/#3)

## Жук Юлия

Доклад: *Сборка мусора +*

Тест: *13.5*

Предварительная оценка: `4`

[Подсчёт слов - ОК](/2017.java/results/zhuk/)

[Фабрика - ОК](/2017.java/results/zhuk/#2)

[Сапёр - ОК](/2017.java/results/zhuk/#3)

## Костылева Виктория

Доклад: *Коллекции +*

Тест: *17*

Предварительная оценка: `4`

[Подсчёт слов - ОК](/2017.java/results/kostyleva/)

[Игра - ОК](/2017.java/results/kostyleva/#2)

[Фабрика - ОК](/2017.java/results/kostyleva/#3)

## Миляев Иван

Доклад: *Функции, лямбды +*

Тест: *17.5*

Предварительная оценка: `5`

[Подсчёт слов - ОК](/2017.java/results/milyaev/)

[Фабрика - ОК](/2017.java/results/milyaev/#2)

[Сапёр - ОК](/2017.java/results/milyaev/#3)

[Чат - ОК](/2017.java/results/milyaev/#4)

## Турчинович Михаил

Доклад: *Jenerics +*

Тест: *18*

Предварительная оценка: `5`

[Подсчёт слов - ОК](/2017.java/results/turchinovich/)

[Фабрика - ОК](/2017.java/results/turchinovich/#2)

[Сапёр - ОК](/2017.java/results/turchinovich/#3)

[Чат - ОК](/2017.java/results/turchinovich/#4)


## Харисов Дамир

Доклад: *Массивы +*

Тест: *14.5*

Предварительная оценка: `5`

[Подсчет слов - ОК](/2017.java/results/kharisov/)

[Фабрика - ОК](/2017.java/results/kharisov/#2)

[Тетрис - ОК](/2017.java/results/kharisov/#3)

[Чат - ОК](/2017.java/results/kharisov/#4)
