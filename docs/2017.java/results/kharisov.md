# Харисов Дамир.

## Подсчёт слов (Задача 1). ОК.

**Код** - ОК.

- [ОК] Вместо `int[3]` для хранения результатов лучше использовать отдельный класс с тремя именованными полями, getter'ами и setter'ами.

- Для итерации по коллекциям можно использовать "foreach":
```Java
for (String f: files) {
	...
}
```

- [хорошо:)] "Архитектурная" ошибка: класс `CommandLineParser` зависит от класса `Counter`, хотя ответственность CommandLineParser'а - только работа в аргументами командной строки.

- [ОК] Вместо int для булевых флагов (`foundFlag`) лучше использовать boolean.

- [ОК] Вместо `String currWord` следует использовать `StringBuilder` или `StringBuffer`.

- [ОК] По условию задачи, в выводе программы должно присутствовать имя файла, а не его порядковый номер.

- [Пожелание] Переменные примитивных типов инициализируются значениями по умолчанию, поэтому явная инициализация в конструкторе лишняя:
```Java
public resultData() {
	fileName = ""; 
	stringsCount = 0;
	wordsCount = 0;
	charsCount = 0;
}   
```

**Тесты** - ОК.

## Фабрика (Задача 2). ОК.

**Фабрика** - ОК.

**Classloader** - ОК

**GUI** - ОК

## Тетрис (Задача 3). ОК.

**MVC** - ОК.

- [ОК] `BoardView` должен реализовывать интерфейс `View`, в котором должны быть объявлены методы, необходимые для работы с View
(repaintAfterMove, repaintAfterDestruction, repaintAfterInsertion ...)

- [ОК] `Controller` должен работать с интерфейсом `View`.

## Чат (Задача 4). ОК.

**Object serialization chat** - ОК.

**XML chat** - ОК.
