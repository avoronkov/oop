# Оспанов. Подсчет слов.

Пожелания:

Использовать try-with-resource для работы с потоками ввода-вывода.

Реализовать WordReader для чтения по словам (`Iterable<String>`, `Iterator<String>`)
