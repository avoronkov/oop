# Материалы практических занятий по курсу ООП (Весна 2020)

Здесь представлены материалы семинарских занятий.

Семинарист: Воронков Александр (`a.voronkov@g.nsu.ru`).

## Задачи
[Задача 1. Подсчёт фраз.](/18201.java/task1/) _(обновлено 26 фев 2020)_

[Задача 2. Стековый язык программирования.](/18201.java/task2/) _(обновлено 26 фев 2020)_

[Задача 3. Игра.](/18201.java/task3/) _(обновлено 26 фев 2020)_

[Задача 4. Фабрика.](/18201.java/task4/) _(обновлено 26 фев 2020)_

[Задача 5. Сеть.](/18201.java/task5/) _(обновлено 26 фев 2020)_

## Семинары

[Cеминар 22.03.2020 (MVC)](/18201.java/seminar7/)

[Cеминар 06.04.2020 (Multithreading)](/18201.java/seminar8/)

[Cеминар 13.04.2020 (Network)](/18201.java/seminar9/)

[Cеминар 20.04.2020 (Games)](/18201.java/seminar10/)

## Доп. материалы

[Темы для докладов (!)](/18201.java/topics/)

[Процесс сдачи задач](gitlab/) (обновлено 09 сентября)

[Материалы лекций](https://sites.google.com/site/nguoop/)

[Список дополнительной литературы](https://sites.google.com/site/nguoop/spisok-dopolnitelnoj-literatury-1)

["Воспитай в себе обезьяну!" (М. Дорофеев)](https://2016.codefest.ru/lecture/1116)

[Что такое красивый код, и как его писать?](https://habrahabr.ru/post/266969/)

[Презентация "Linux for programmers"](/2018.java/linux-for-programmers.pdf)
