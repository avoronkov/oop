# Теоретические вопросы по курсу Основы ООП (C++)

1 Сформулируйте основную идею объектно-ориентированного программирования. В чем его преимущества перед процедурным подходом?

2 Принцип абстракции. Средства языка С++ поддерживающие этот принцип. Барьер абстракции.

3 Инкапсуляция. Средства С++ для поддержки инкапсуляции.

4 Понятие объекта. Варианты отношений между объектами. Взаимосвязь поведения и состояния объекта.

5 Понятие класса. Варианты отношений между классами.

6 Абстрактные классы и интерфейсы. Для чего они нужны? В чём между ними разница?

7 В приведенном классе отметьте переменные и методы, обеспечивающие состояние и поведение объектов класса:
```C++
class Detail {
	const int serial;
	Detail *pcomponents;
	double weight;
	static const int ARTICLE;
protected:
	Detail();
	Detail(const Detail &);
public:
	Detail(int);
	Detail & operator=(const Detail&);
	static void performAction();
	int getSerial() const;
	void setWeight(const double &);
	double getWeight() const;
	void doSomething();
	bool hasNextComponent() const;
	Detail * nextComponent();
	~Detail();
};
```

8 В приведенном выше примере отметьте, какие методы являются конструкторами, деструкторами, модификаторами, селекторами, итераторами.

9 Примитивные типы. Размеры примитивных типов.

10 Напишите функцию swap, которая обменивает значения двух переменных типа double а) используя в качестве типа аргументов функции указатели на double,
б) использую ссылки на double.

11 Типы памяти в программах на C++.

12 Что распечатает слудующая программа на C++? Что вам кажется потенциально опасным, некрасивым? Почему?
```
#include <iostream>
using namespace std;

class A {
public:
	A() { cout << "+"; }
	A(const A & a) { cout << "#"; }
	~A() { cout << "-"; }
};

class B {
	A a;
public:
	B(A tmp) {
		cout << "B";
		a = tmp;
	}
	A & getA() { return a; }
};

A global;
int main() {
	cout << "begin";
	A * pa = new A();
	A local(*pa);
	B b(local);
	local = b.getA();
	cout << "finish";
}
```

13 Что распечатает следующая программа на C++? Что вам кажется потенциально опасным, некрасивым? Почему?
```
#include <iostream>
#include <stdlib.h>

class A {
	static int counter;
	int i;
public:
	A() { i = counter++; }
	~A() { counter--; }
	friend ostream& operator<<(ostream&, const A&);
};

ostream& operator<<(ostream& out, const A& a) {
	return out << a.i;
}

int A::counter = 0;
A global;

int main() {
	cout << global << endl;
	A *p2A = static_cast<A*>(calloc(5, sizeof(A)));
	A localArray[2];
	A * pA = new A[6];
	cout << p2A[2] << endl;
	cout << localArray[1] << endl;
	cout << *pA << endl;
	delete[] pA;
	free(p2A);
	A local;
	cout << local << endl;
}
```
