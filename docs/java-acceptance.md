# Задачи

1 **Амикишиева Руслана** `Fine (4)` : "Подсчёт слов" - OK, "Калькулятор" - [ОК](java_task2_results/amikishieva_task2), Сапёр - [ОК](java_task3/amikishieva)

2 **Демидов Сергей** `Fair (3)`: "Подсчёт слов" - был, Калькулятор - [окей...](java_task2/demidov)  "Танки" - [let it be...](java_task3/demidov)

3 **Дудаев Александр** `OK (5)` : "Подсчёт слов" - ОК, "Калькулятор" - [ОК](java_task2_results/dudaev_task2), Сапёр - [ОК](java_task3/dudaev)

4 **Камбалина Алена** `Fine (4-)`: "Подсчёт слов" - ОК, "Калькулятор" - [ОК](java_task2_results/kambalina_task2), Сапёр - [ОК](java_task3/kambalina)

5 **Катчик Никита** `ОК (5)`: "Подсчёт слов" - ОК, "Калкулятор" - ОК, "Сапёр" - [в процессе](java_task3/katchik), "Фабрика" - ОК, "Чат" - [ОК](java_task5/katchik)

6 **Малафеев Виталий** `Fair (3)` : "Подсчёт слов" - ОК, "Калькулятор" - [ОК](java_task2_results/malafeev_task2), "Тетрис" - [ОК](java_task3/malafeev)

7 **Матвеев Андрей** `OK (5)`: "Подсчёт слов" - OK, "Калкулятор" - [ОК](java_task2_results/matveev_task2), "Тетрис" - ОК, "Фабрика" - [ОК](java_task4/matveev), "Чат" - [ОК](java_task5/matveev)

8 **Оспанов Даулет** `Fair (3)`: "Подсчёт слов" - [ОК](java_task1/ospanov), "Калькулятор" [ОК](java_task2_results/ospanov), "Сапёр" [Fine](java_task3/ospanov)

9 **Свищев Алексей** `ОК (5)`: "Подсчёт слов" - ОК, "Калькулятор" - ОК, "Сапёр" - [в процессе](java_task3/svischev), "Фабрика" - ОК

10 **Хаджиев Матвей** `Fine (4-)`: "Подсчёт слов" - ОК, Калькулятор - ОК. Пакман - не удался, Сапёр - ОК

11 **Шаченко Никита** `ОК (5)`: "Подсчёт слов" - ОК, Калькулятор - [ОК](java_task3_results/shachenko_task2), Сапёр - [ОК](java_task3/shachenko), Фабрика - [ОК](java_task4/shachenko.md)

