current_dir = "18201.java"
current_group_dir = "docs/" + current_dir

USERS = [
    ('baranov', ['18201_oop_baranov']),
    ('lyashuk', ['18201_oop_lyashuk']),
    ('getmanova', ['18201_oop_getmanova']),
    ('gnezdilova', ['18201_oop_gnezdilova']),
    ('golovtsov', ['18201_oop_golovtsov']),
    ('lobanov', ['18201_oop_lobanov']),
    ('michurov', ['18201_oop_michurov']),
    ('kigel', ['18201_oop_kigel']),
    ('kononov', ['18201_oop_kononov']),
    ('mustafin', ['18201_oop_mustafin']),
    ('reshetnyak', ['18201_oop_reshetnyak']),
    ('chernyj', ['18201_oop_chernyj']),
]

charts_dir = 'charts.18201';
