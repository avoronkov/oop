#!/bin/bash


cmd="$@"

err=0
n=0

run_test()
{
	local libs="$1"
	local targets="$2"
	local output="$3"
	local stderr="${4:-}"
	local exitcode="${5:-0}"

	let n=$(( n + 1 ))

	cp -f "$libs" libs.txt
	cp -f "$targets" targets.txt
	eval "$cmd" > temp.output.txt 2> temp.stderr.txt
	local ret=$?
	if [ "$ret" != "$exitcode" ]; then
		let err=$(( err + 1 ))
		echo "[!] test $n failed: incorrect exit code: actual $ret, expected $exitcode" >&2
		return 1
	fi

	if [ -n "$output" ]; then
		if ! diff -u -w temp.output.txt "$output"; then
			let err=$(( err + 1 ))
			echo "[!] test $n failed: incorrect output" >&2
			return 1
		fi
	fi

	if [ -n "$stderr" ]; then
		if ! diff -u -w temp.stderr.txt "$stderr"; then
			let err=$(( err + 1 ))
			echo "[!] test $n failed: incorrect stderr" >&2
			return 1
		fi
	fi
}

run_test './testdata/libs01.txt' './testdata/targets01.txt' './testdata/output01.txt'
run_test './testdata/libs02.txt' './testdata/targets02.txt' './testdata/output02.txt'
run_test './testdata/libs03.txt' './testdata/targets03.txt' './testdata/output03.txt'
run_test './testdata/libs04.txt' './testdata/targets04.txt' './testdata/output04.txt'
run_test './testdata/libs05.txt' './testdata/targets05.txt' './testdata/output05.txt'
run_test './testdata/libs06.txt' './testdata/targets06.txt' './testdata/output06.txt'
run_test './testdata/libs07.txt' './testdata/targets07.txt' '' './testdata/stderr07.txt' 1

if [ "$err" != 0 ]; then
	echo "$err tests failed"
	exit 1
fi

echo "OK! ($n tests passed)"
