#!/bin/bash

cmd="$@"

err=0
n=0

run_test()
{
	local desc="$1"
	local args="$2"
	local output="$3"

	let n=$(( n + 1 ))

	echo "$n. $desc"
	echo "$cmd $args"
	eval "$cmd $args" > output.txt
	local ret="$?"
	if [ "$ret" != 0 ]; then
		let err=$(( err + 1 ))
		echo "[!] test $n failed: exit code = $ret" >&2
		return 1
	fi

	perl -ple '$_ = int($_)' output.txt > temp.txt

	if ! diff -u temp.txt "$output"; then
		let err=$(( err + 1 ))
		echo "[!] test $n failed: incorrect output" >&2
		return 1
	fi

	echo "[OK] test $n"
}

run_test 'cycle print' '< ./testdata/input01.txt' './testdata/output01.txt'

run_test 'factorial 5' '< ./testdata/input02.txt' './testdata/output02.txt'

run_test '2 + 3' '< ./testdata/input03.txt' './testdata/output03.txt'

run_test 'conditional print' '< ./testdata/input05.txt' './testdata/output05.txt'

run_test 'min(5, 7)' '< ./testdata/input06.txt' './testdata/output06.txt'

run_test 'min(7, 5)' '< ./testdata/input07.txt' './testdata/output07.txt'

run_test 'nested cycle print' '< ./testdata/input09.txt' './testdata/output09.txt'

run_test 'define min(5, 7)' '< ./testdata/input10.txt' './testdata/output10.txt'

run_test 'define min(7, 5)' '< ./testdata/input11.txt' './testdata/output11.txt'

run_test 'define factorial' '< ./testdata/input12.txt' './testdata/output12.txt'

run_test 'prime numbers' '< ./testdata/09.prime.txt' './testdata/09.prime.out.txt'

if [ "$err" != 0 ]; then
	echo "$err/$n tests failed"
	exit 1
else
	echo "$n tests OK"
fi
