#include "catch.hpp"

#include <list>

#include DEFINES

TEST_CASE("basics", "Tritset") {
	const TRITSET a{UNKNOWN, UNKNOWN, UNKNOWN, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE};
	const TRITSET b{UNKNOWN, TRUE, FALSE, UNKNOWN, TRUE, FALSE, UNKNOWN, TRUE, FALSE};

	REQUIRE(a.length() == 9);
	REQUIRE(b.length() == 9);

	const TRITSET aandb{UNKNOWN, UNKNOWN, FALSE, UNKNOWN, TRUE, FALSE, FALSE, FALSE, FALSE};
	const TRITSET aorb{UNKNOWN, TRUE, UNKNOWN, TRUE, TRUE, TRUE, UNKNOWN, TRUE, FALSE};
	const TRITSET nota{UNKNOWN, UNKNOWN, UNKNOWN, FALSE, FALSE, FALSE, TRUE, TRUE, TRUE};

	CHECK((a & b) == aandb);
	CHECK((a | b) == aorb);
	CHECK((NOT a) == nota);

	TRITSET a1 = a;
	a1 &= b;
	CHECK(a1 == aandb);

	TRITSET a2 = a;
	a2 |= b;
	CHECK(a2 == aorb);
}

TEST_CASE("indexes", "Tritset") {
	const TRITSET a{UNKNOWN, TRUE, FALSE};
	TRITSET b(a);

	CHECK(a == b);
	CHECK(b[0] == UNKNOWN);
	CHECK(b[1] == TRUE);
	CHECK(b[2] == FALSE);

	b[1] = FALSE;
	CHECK(b[0] == UNKNOWN);
	CHECK(b[1] == FALSE);
	CHECK(b[2] == FALSE);
	CHECK(a != b);
}

TEST_CASE("resize", "Tritset") {
	TRITSET a;
	const TRITSET & ra = a;
	// REQUIRE(ra.length() == 0);

	a[9] = TRUE;
	REQUIRE(ra.length() == 10);
}

TEST_CASE("cardinality", "Tritset") {
	const TRITSET a{UNKNOWN, UNKNOWN, FALSE, FALSE, FALSE, TRUE};
	auto crd = a.cardinality();
	CHECK(crd[TRUE] == 1);
	CHECK(crd[FALSE] == 3);
	CHECK(crd[UNKNOWN] == 2);

	CHECK(a.cardinality(TRUE) == 1);
	CHECK(a.cardinality(FALSE) == 3);
	CHECK(a.cardinality(UNKNOWN) == 2);
}

TEST_CASE("for-each", "Tritset") {
	TRITSET a{UNKNOWN, UNKNOWN, FALSE, FALSE, FALSE, TRUE};
	std::list<TRIT> exp{UNKNOWN, UNKNOWN, FALSE, FALSE, FALSE, TRUE};
	int l = exp.size();
	REQUIRE(a.length() == l);

	int i = 0;
	for (auto t: a) {
		REQUIRE (i < l);
		INFO("Check position " << i);
		CHECK(t == exp.front());
		exp.pop_front();
		i++;
	}
}
