#include "trit.h"
#include "tritset.h"

#define TRIT Trit

#define TRUE Trit::T
#define FALSE Trit::F
#define UNKNOWN Trit::U

#define TRITSET Tritset

#define NOT ~
