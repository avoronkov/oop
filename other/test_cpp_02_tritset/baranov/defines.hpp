#pragma once

#include "Trit.h"
#include "Tritsets.h"

#define TRIT Trit

#define TRUE Trit::True
#define FALSE Trit::False
#define UNKNOWN Trit::Unknown

#define TRITSET TritSet

#define NOT !
