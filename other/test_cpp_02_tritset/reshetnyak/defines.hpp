#pragma once

#include "tritset.h"

#define TRIT Trit

#define TRUE Trit::True
#define FALSE Trit::False
#define UNKNOWN Trit::Unknown

#define TRITSET TritSet

#define NOT ~
