#include "trit.h"
#include "tritset.h"

#define TRIT Trit

#define TRUE Trit::True
#define FALSE Trit::False
#define UNKNOWN Trit::Unknown

#define TRITSET Tritset

#define NOT ~
