#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include DEFINES

#include <tuple>


typedef std::tuple<TRIT, TRIT, TRIT> example;

TEST_CASE("and", "Trit") {
	auto examples = {
		example(TRUE, TRUE, TRUE),
		example(TRUE, FALSE, FALSE),
		example(TRUE, UNKNOWN, UNKNOWN),

		example(FALSE, TRUE, FALSE),
		example(FALSE, FALSE, FALSE),
		example(FALSE, UNKNOWN, FALSE),

		example(UNKNOWN, TRUE, UNKNOWN),
		example(UNKNOWN, FALSE, FALSE),
		example(UNKNOWN, UNKNOWN, UNKNOWN),
	};
	for (auto e : examples) {
		TRIT a, b, c;
		std::tie(a, b, c) = e;
		const TRIT & ra = a;
		const TRIT & rb = b;
		const TRIT & rc = c;
		CHECK((ra & rb) == rc);
	}
}

TEST_CASE("or", "Trit") {
	auto examples = {
		example(TRUE, TRUE, TRUE),
		example(TRUE, FALSE, TRUE),
		example(TRUE, UNKNOWN, TRUE),

		example(FALSE, TRUE, TRUE),
		example(FALSE, FALSE, FALSE),
		example(FALSE, UNKNOWN, UNKNOWN),

		example(UNKNOWN, TRUE, TRUE),
		example(UNKNOWN, FALSE, UNKNOWN),
		example(UNKNOWN, UNKNOWN, UNKNOWN),
	};
	for (auto e : examples) {
		TRIT a, b, c;
		std::tie(a, b, c) = e;
		const TRIT & ra = a;
		const TRIT & rb = b;
		const TRIT & rc = c;
		CHECK((ra | rb) == rc);
	}
}

TEST_CASE("not", "Trit") {
	auto examples = {
		std::pair<TRIT, TRIT>(TRUE, FALSE),
		std::pair<TRIT, TRIT>(FALSE, TRUE),
		std::pair<TRIT, TRIT>(UNKNOWN, UNKNOWN),
	};
	for (auto e : examples) {
		TRIT a, b;
		std::tie(a, b) = e;
		const TRIT & ra = a;
		const TRIT & rb = b;
		CHECK((NOT ra) == rb);
	}
}
