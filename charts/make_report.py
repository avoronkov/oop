#!/usr/bin/env python3

import logging
import os
import requests
import subprocess
import sys
import shutil

import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

from settings import USERS, charts_dir

URL = 'https://api.bitbucket.org/2.0/repositories/'

GITLAB_URL = 'https://gitlab.com/api/v4'

BASE_REPO_DIRS = []
BASE_DIR = 'repos'


class TokenException(Exception):
    pass


def pull(repo_path):
    try:
        if os.path.exists(os.path.join(repo_path, '.git')):
            subprocess.check_call(["git", "pull"], cwd=repo_path)
            return True
        elif os.path.exists(os.path.join(repo_path, '.hg')):
            subprocess.check_call(["hg", "pull"], cwd=repo_path)
            subprocess.check_call(["hg", "update"], cwd=repo_path)
            return True
        else:
            logging.warning("No .git or .hg found in %s" % (repo_path))
    except Exception as e:
        logging.exception("Failed to pull in %s" % repo_path)
    return False


def clone(repo_url, repo_path):
    if "hg@" in repo_url:
        subprocess.check_call(["hg", "clone", repo_url, repo_path])
    elif "git@" in repo_url:
        subprocess.check_call(["git", "clone", repo_url, repo_path])
    else:
        raise Exception("Unknown repo type: %s" % repo_url)


class Repo:
    def __init__(self, repo_id, repo_ssh, commit):
        self.repo_id = repo_id
        self.repo_ssh = repo_ssh
        self.repo_commit = commit


class ReposProcessor:
    def __init__(self):
        self._userdata = []
        self._token = os.environ["TOKEN"]

    def process_user_gitlab(self, user, gids):
        token = self._token
        repos = []
        for gid in gids:
            req_url = "{0}/groups/{1}".format(GITLAB_URL, gid)
            logging.warning("url: {0}".format(req_url))
            r = requests.get(req_url, headers={'Private-Token': token})
            json_data = r.json()
            if 'error' in json_data:
                raise TokenException(json_data['error'])
            # logging.warning("json_data = {0}".format(json_data))
            for proj in json_data['projects']:
                print("{0} => {1} ({2})".format(proj['path'], proj['ssh_url_to_repo'], proj['id']))
                repos.append(Repo(proj['id'], proj['ssh_url_to_repo'], self._get_repo_master_commit(proj['id'])))
        self.prep(user, repos)

    def _get_repo_master_commit(self, proj_id):
        req_url = "{0}/projects/{1}/repository/branches".format(GITLAB_URL, proj_id)
        r = requests.get(req_url, headers={'Private-Token': self._token})
        json_data = r.json()
        if 'error' in json_data:
            raise TokenException(json_data['error'])
        for branch in json_data:
            if branch['name'] == 'master':
                return branch['commit']['id']
        return None

    def prep(self, user, repos):
        d = os.path.join(BASE_DIR, user)
        if not os.path.exists(d):
            os.makedirs(d)
        BASE_REPO_DIRS.append(d)
        for r in repos:
            reponame = os.path.basename(r.repo_ssh)
            rp = os.path.join(d, reponame)
            if os.path.exists(rp):
                cur_commit = self._get_last_commit(rp)
                if r.repo_commit == cur_commit:
                    print("Skip update repo {0}: already up2date ({1})".format(r.repo_id, cur_commit))
                elif not pull(rp):
                    shutil.rmtree(rp)
                    clone(r.repo_ssh, rp)
            else:
                clone(r.repo_ssh, rp)

            last_commit = self._get_last_commit(rp)
            pipeline_status = self._get_pipeline_status(r.repo_id)
            self._userdata.append((user, r.repo_ssh, last_commit, pipeline_status))

    def _get_last_commit(self, repo_path):
        try:
            return subprocess.check_output(['git', 'rev-parse', 'HEAD'], cwd=repo_path).decode('utf-8').strip()
        except Exception as e:
            logging.warning(e)
            return "none"

    def _get_pipeline_status(self, repo_id):
        req_url = "{0}/projects/{1}/pipelines".format(GITLAB_URL, repo_id)
        r = requests.get(req_url, headers={'Private-Token': self._token})
        json_data = r.json()
        if 'error' in json_data:
            raise TokenException(json_data['error'])
        if len(json_data) == 0:
            return "none"
        return json_data[0]['status']

    def dump_meta_info(self, output="meta.txt"):
        with open(output, "w") as f:
            for (user, repo, commit, status) in sorted(self._userdata):
                print("{} {} {} {}".format(user, repo, commit, status), file=f)


def main():
    names = sys.argv[1:]

    proc = ReposProcessor()
    for user, uids in USERS:
        if len(names) > 0 and user not in names:
            continue
        try:
            logging.warning("process user %s %s" % (user, uids))
            proc.process_user_gitlab(user, uids)
        except TokenException as e:
            raise
        except Exception:
            logging.exception("Failed to process {}".format(user))
    if len(names) > 0:
        logging.info("skipping charts generation")
        return
    logging.warning("base repos dirs: %s" % BASE_REPO_DIRS)
    subprocess.check_call(["./gen_report", charts_dir] + BASE_REPO_DIRS)
    if len(names) == 0:
        proc.dump_meta_info()


if __name__ == '__main__':
    try:
        main()
    except Exception:
        logging.exception("Error")
