<html>
<head>
	<title>%NAME% activity</title>
	<script src="../js/Chart.bundle.js"></script>
	<script src="../js/moment.min.js"></script>
	<script src="../js/utils.js"></script>
</head>

<body>
	<h1>%NAME%</h1>
	<div style="margin: 3em;">
		<canvas id="canvas1" width="800" height="400"></canvas>
		<canvas id="canvas2" width="800" height="400"></canvas>
	</div>
	<script>
var dateFormat = 'YYYY-MM-DD';

